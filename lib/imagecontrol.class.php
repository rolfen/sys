<?

class ImageControl extends Control {
	function __construct($name, $specs) {
		$this->name = $name;
		$this->masterColumName = $name."_original";
		$this->specs = $specs;
	}
	function tableColumns() {
		// return the column names that this image uses in the table, in an array
		foreach($specs['samples'] as $sampleKey => $sampleSpec) {
			$cols[] = $name.'_'.$sampleKey;
		}
		return($cols);
	}
	function preload($record) {

	}
	function put($original) {
		$this->data->{$this->name.'_original'} = $original;
		foreach($this->specs['samples'] as $sampleKey => $sample) {
			$this->data->{$this->name.'_'.$sampleKey} = resize(
				$original, 
				$sample['dimensions'],
				isset($sample['options']) ? $sample['options'] :  null
			);
		}
	}
	function dump() {
		return((array) $this->data);
	}
}

?>