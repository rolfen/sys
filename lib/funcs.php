<?php

// TODO:
// think about this: getRow() (and others) must return empty Record(), not false, when nothing found, so that we don't need to do type checking later

// ============ strings ================

function smart_truncate($string, $limit, $break=" ", $pad="...")  {
	// truncates string to the first $break after $limit characters, then adds $pad to the end
	// Original PHP code by Chirp Internet: www.chirp.com.au 
	// Please acknowledge use of this code by including this header. 
	// return with no change if string is shorter than $limit
	$string = strip_tags($string);
	if(strlen($string) <= $limit) return $string;
	// is $break present between $limit and the end of the string?
	if(false !== ($breakpoint = strpos($string, $break, $limit))) {
		if($breakpoint < strlen($string) - 1) {
			$string = substr($string, 0, $breakpoint) . $pad; 
		}
	} 
	return $string; 
}

function escape_js_string($string) {
	// escapes single quotes and double quotes for use in generated javascript strings
	return (str_replace(array("'",'"'), array('\x27','\x22'), $string));
}


// ============= Objects / Arrays ============

function copy_properties($from, $to, $fields = null) {
	// copies properties/elements (overwrites duplicates)
	// can take arrays or objects 
	// if fields is set (an array), will only copy keys listed in that array
	// returns $to with the added/replaced properties/keys
	$from_array = is_array($from) ? $from : get_object_vars($from);
	foreach($from_array as $key => $val) {
		if(!is_array($fields) or in_array($key, $fields)) {
			if(is_object($to)) {
				if(trim($key) != '') {
					$to->$key = $val;
				} else {
					trigger_error("Empty string cannot be used as object property name. Skipping one property/element.");
				}
			} else {
				$to[$key] = $val;
			}
		}
	}
	return($to);
}

function array_merge_recursive_simple() {
	// Recursive version of array_merge(). 
	// if it finds the same numeric key in arrays to merge, the keys will be merged,
	// as opposed to appended in the native array_merge_recursive().
	// variable arg list
    if (func_num_args() < 2) {
        trigger_error(__FUNCTION__ .' needs two or more array arguments', E_USER_WARNING);
        return;
    }
    $arrays = func_get_args();
    $merged = array();
    while ($arrays) {
        $array = array_shift($arrays);
        if (!is_array($array)) {
            trigger_error(__FUNCTION__ .' encountered a non array argument', E_USER_WARNING);
            return;
        }
        if (!$array)
            continue;
        foreach ($array as $key => $value)
            if (is_string($key))
                if (is_array($value) && array_key_exists($key, $merged) && is_array($merged[$key]))
                    $merged[$key] = call_user_func(__FUNCTION__, $merged[$key], $value);
                else
                    $merged[$key] = $value;
            else
                $merged[] = $value;
    }
    return $merged;
}

function array_remove_empty($array) {
	// recursively removes empty values from array
	$out = array();
	foreach($array as $key => $val) {
		if(!empty($val)) {
			if(is_array($val)) {
				$out[$key] = array_remove_empty($val);
			} else {
				$out[$key] = $val;
			}
		}
	}
	return($out);
}


// ============= Image handling ============

function resize($imagedata, $given_size, $options = array()) {
	// resize image, constraining image dimentions inside $given_size[w] and $given_size[h] pixels

	// if imagick is available, GIF animations will not be lost during resize

	// you don't need to set both - you can just set one and the other one will be unconstrained
	// if a number is given instead of an array, image will be constrained inside a square with both sides measuring $given_size pixels
	

	// In addition to that, different sets of constraining dimentions can be given for different "conditions"
	// currently these two conditions are supported: portrait and landscape (USE THEM TOGETHER!)
	// this is an example of how to use them:
	/*
		$given_size = array(
			"portrait" => array(	// this will be used if the $imagedata is detected as "portrait" (height > width)
				"h" => "200"		// height: 200px max, widht: unconstrained
			),
			"landscape" => array(	// and this for "landscape" images (width >= height)
				"h" =>  "400",
				"w" => "300"
			)
		)
	*/

	// available $options[]:
	// allow_upsize : If false, never resize given_image to something bigger. Default: true
	// progressive_jpeg : bool: output progressive (interlaced) jpeg. Unset for auto (=progressive for images bigger than 30 000 pixels, but it doesn't seem to work... set manually if possible)
	// jpeg_quality : (from 0 to 100, defaults to 85)


	// requires is_png()

	if(!is_array($options)) {
		// backwards compat - support old bool argument
		$tmp = array();
		$tmp['allow_upsize'] = $options;
		$options = $tmp;
	}

	// default options
	isset($options['allow_upsize']) or $options['allow_upsize'] = true;
	// other options:
	// $options['progressive_jpeg'] bool
	// $options['jpeg_quality'] (from 0 to 100, defaults to 85)

	extension_loaded('gd') or dl("php_gd2");

	// load image
	if(!empty($imagedata)) {
		$image = imagecreatefromstring($imagedata);
        imagealphablending($image , false);
        imagesavealpha($image , true);
		if($image === false) {
			// oops, not an image most probably
			trigger_error("Unable to understand given image data");
			return;
		}
	} else {
		trigger_error("Empty string given as image data");
		return;
	}

	$src_width = imagesx($image); 
	$src_height = imagesy($image);

	$src_ratio = $resized_ratio = (float) 0;
	$src_ratio = (float) $src_height / (float) $src_width;

	$given_size_arg = $given_size;

	if(is_array($given_size_arg) and isset($given_size_arg['portrait']) and $src_ratio > 1) {
		$given_size = $given_size_arg['portrait'];
	}

	if(is_array($given_size_arg) and isset($given_size_arg['landscape']) and ($src_ratio <= 1) and ($src_ratio > 0) ) {
		$given_size = $given_size_arg['landscape'];
	}

	if (!is_array($given_size) and is_numeric($given_size)) {
		$size = array();
		$size['h'] = $size['w'] = (int) $given_size;
	} else if (is_array($given_size) and (isset($given_size["h"]) or isset($given_size["w"]))) {
		!isset($given_size["h"]) and $given_size["h"] = "99999999999999";
		!isset($given_size["w"]) and $given_size["w"] = "99999999999999";
		$size = $given_size;
	} else {
		trigger_error("Size argument is not of valid form");
		return;			
	}

	if ($size['h'] == 0 or $size['w'] == 0) {
		// avoid divide by 0
		trigger_error("Size cannot contain 0");
		return;		
	}

	$resized_ratio = (float) $size['h'] / (float) $size['w'];

	$ref_axis = ($resized_ratio > $src_ratio) ? "w" : "h" ;

	// calculate resize ratio
	if ($ref_axis == 'w') {
		if($options['allow_upsize'] or ($src_width > $size['w'])) {
			$resized_width = $size['w'];
			$resized_height = (int) ($size['w'] * $src_ratio); 
		} else {
			$resized_width = $src_width;
			$resized_height = $src_height; 			
		}
	} else {
		if($options['allow_upsize'] or ($src_height > $size['h'])) {
			$resized_height = $size['h'];
			$resized_width = (int) ($size['h'] / $src_ratio);
		} else {
			$resized_width = $src_width;
			$resized_height = $src_height; 			
		}
	}
    
	if (((bool)preg_match('/\x00\x21\xF9\x04.{4}\x00(\x2C|\x21)/s', $imagedata, $m)) and class_exists("Imagick")) {
		// if we are trying to resize a GIF and we have Imagick, use Imagick to keep animation
		$imagick = new Imagick();
		$imagick->readImageBlob($imagedata);
		$imagick = $imagick->coalesceImages();
		do {
			$imagick->resizeImage($resized_width, $resized_height, Imagick::FILTER_BOX, 1);
		} while ($imagick->nextImage());
		$imagick = $imagick->deconstructImages();
		$thumbdata = $imagick->getImagesBlob();
		$imagick->clear();
		$imagick->destroy();
	} else {
		// use the old and simpler resize to JPEG method
		$thumb = imagecreatetruecolor($resized_width, $resized_height);
        imagealphablending($thumb , false);
        imagesavealpha($thumb , true);
		imagecopyresampled($thumb, $image, 0, 0, 0, 0, $resized_width, $resized_height, $src_width, $src_height);	
		
		// if($allow_upscale or ($thumbwidth < $imagewidth) or ($thumbheight < $imageheight) ) {
		// 	// generate thumbnail
		// 	$thumb = imagecreatetruecolor($thumbwidth,$thumbheight);
		// 	imagecopyresampled($thumb, $image, 0, 0, 0, 0, $size['x'], $size['y'], max($imagewidth, $size['x']), $imageheight);		
		// } else {
		// 	// do not resize...
		// 	$thumb = imagecreatetruecolor($imagewidth,$imageheight);
		// 	imagecopyresampled($thumb, $image, 0, 0, 0, 0, $imagewidth, $imageheight, $imagewidth, $imageheight);			
		// }

		// compress and return (imagejpeg() outputs to the browser, so we need to catch the output with ob_)
		ob_start();
		if(is_png($imagedata)) {
			// generate png
			$output_res = imagepng($thumb);
		} else {
			// generate jpeg
			if(isset($options['progressive_jpeg']) ? $options['progressive_jpeg'] : (($resized_width * $resized_height) > 30000) ) {
				imageinterlace($thumb, true);
			}			
			$output_res = imagejpeg($thumb, NULL, isset($options['jpeg_quality']) ? $options['jpeg_quality'] : 85);
		}
		if($output_res) {
			$thumbdata = ob_get_contents();
			ob_end_clean();
			imagedestroy($image);
			imagedestroy($thumb);
		} else {
			ob_end_clean();
			trigger_error("Failed to generate output");
		}
	}
	// spit it out
	if (isset($thumbdata) and !empty($thumbdata)) {
		return($thumbdata);
	} else {
		trigger_error("Nothing to return");
		return;
		// I guess we better return nothing at all - to avoid potential confusion (something went wrong)
	}
}



function crop($image, $width, $height, $offset_left, $offset_top, $scale){
	// crop and scale image
	// image: original image to crop
	// width: width of the crop area
	// height: height of the crop area
	// offset_left: x coordinate of the upper left corner (with reference to the same corner of original image)
	// offset_top: y coordinate of the uppoer left corner (with reference to the same corner of original image)
	// scale is like a zoom. If it's 2, for example, every pixel in $image will result in 2 pixels in the returned image

	$newImageWidth = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	$source = imagecreatefromstring($image);
	imagecopyresampled($newImage,$source,0,0,$offset_left,$offset_top,$newImageWidth,$newImageHeight,$width,$height);
	ob_start();
	if( imagejpeg($newImage) ) {
		$imageData = ob_get_contents();
		ob_end_clean();
		return $imageData;
	} else {
		ob_end_clean();
	}
}

function get_height($image) {
	$img = imagecreatefromstring($image);
	return (imagesy($img));
}

function get_width($image) {
	$img = imagecreatefromstring($image);
	return (imagesx($img));
}

function is_animated_gif($image) {
	return (bool)preg_match('/\x00\x21\xF9\x04.{4}\x00(\x2C|\x21)/s', $image, $m);
}

function is_jpeg(&$pict) {
    return (bool)(bin2hex($pict[0]) == 'ff' && bin2hex($pict[1]) == 'd8');
}

function is_png(&$pict) {
    return (bool)(bin2hex($pict[0]) == '89' && $pict[1] == 'P' && $pict[2] == 'N' && $pict[3] == 'G');
}

/* ============= blocks functions ============= */

/* todo idea for get: write a gets_all function which only sets the given global variables if all of them are defined in the GET array. */


function start_block(&$var) {
	isset($var) or $var = "";
	ob_start();
}

function end_block(&$var) {
	if (isset($var)) {
		$var .= ob_get_clean();
	} else {
		$var = ob_get_clean();
	}
	return($var);
}

/* =========== misc ============== */

function debug($var) {
	// returns argument and prints debug info
	echo "<pre>";
	print_r($var);
	echo "</pre>";
	return($var);
}

function debug_dump($var, $level = 0, $variable_name='') {
	// dumps readable representation of variable, with smart features, such as truncating extra long values
	// recursive
	$type = gettype($var);
	$out = $val = "";
	$tc = "\t"; // tabulation character
	$tp = str_repeat($tc, $level + 1); // tabulation prefix
	$lec = "\r\n"; // line ending character
	switch($type) {
		case "object":
		case "array":
			foreach($var as $arr_key => $arr_val) {
				$val .= debug_dump($arr_val, $level+1, $arr_key);
			}
		break;
		default:
			if(strlen($var) > 280) {
				$var = substr($var,0,280)."[...]";
			}
			$var = strtr($var,array("\r" => '', "\n" => ''));
			$val = $tp.$tc.htmlspecialchars((string) $var).$lec; 
		break;
	}
	$out = "$tp".(!empty($variable_name) ? " $variable_name " : '')."($type) : { $lec$val$tp} $lec";
	if($level == 0) {
		$out = "$lec<pre>$lec $out $lec</pre> $lec";
	} 
	return($out);
}


function ieversion() {
	/*
		returns ie BROWSER MODE (not document mode). 
		Returns -1 if no IE detected.
		Attemps to detect "IE8 Compatibility View" browser mode as IE8 (and not IE7 as advertized by the browser) 
	*/
	$match=preg_match('/MSIE ([0-9]\.[0-9])/',$_SERVER['HTTP_USER_AGENT'],$reg);
	if($match==0) {
		// not IE
		return -1;
	} else {
		$ieVer = floatval($reg[1]);
		if ($ieVer == 7) {
			// Check if it's not 8 running in "compatibility view" (it will advertize itself as IE7.0 but also send "Trident/4.0")
			$match2=preg_match('/Trident\/4/',$_SERVER['HTTP_USER_AGENT'],$reg);
			if ($match2) {
				return (8);
			} else {
				return($ieVer);
			}
		} else {
			return($ieVer);
		}
	}
}


function randstr($length=5) {
	// random string of length $length
	$ret = "";
	for ($i=0; $i<($length); $i++) {
	    $d=mt_rand(1,30)%2;
	    $ret .= $d ? chr(mt_rand(65,90)) : chr(mt_rand(48,57));
	} 
	return $ret;
}


function makeUrlQuery($args) {
	// builds an URL query from key => value pairs
	// (does not prepend "?" at the beginning)
	$url = "";
	foreach($args as $key => $val) {
		$url .= rawurlencode($key).'='.rawurlencode($val).'&';
	}
	$url = rtrim($url, "&");
	return($url);
}

/* =========== database and text/html conversion functions ============== */

function text_to_list($text) {
	// text to html unordinated list... essentially turns each line into a li and wraps it all in ul
	$aTex = preg_split('/[\r\n]+/', $text, -1, PREG_SPLIT_NO_EMPTY);
	if (count($aTex)) {
		$tex  = "<ul>\r\n";
		foreach ( $aTex as $texLine ) {
			$texLine = trim($texLine);
			$texLine = trim($texLine, " -");
			$tex .= "<li>".$texLine."</li>\r\n";
		}
		$tex .= "</ul>";
	}
	return $tex;
}

function list_to_text($list) {
	// html list to text, reverse text_to_list
	$list = strtr($list, array("\r" => "", "\n" => ""));
	$list = strtr($list, array("<ul>" => "", "</ul>" => ""));
	$list = strtr($list, array("</li>" => "\r\n", "<li>" => " - "));
	return $list;	
}

function text_to_html($text, $wrap_in_p = true) {
	// to html... changes double newlines to paragraphs and single newlines to br
	// $html = htmlspecialchars($text);
	$html = strtr($text, array("\r\n\r\n" => "</p><p>"));
	$html = strtr($html, array("\r\n" => "<br/>"));
	$wrap_in_p and $html = '<p>'.$html.'</p>';
	return $html;
}

function html_to_text($html) {
	// from HTML, reverses text_to_html
	$text = strtr($html, array("</p><p>" => "\r\n\r\n"));
	$text = strtr($text, array("<br/>" => "\r\n"));
	if (substr($text, 0, 3) == '<p>' and substr($text, -4, 4) == '</p>') {
		$text = substr($text, 3);
		$text = substr($text, 0, -4);
	}
	return $text;
}

// database

function dbConnect($host, $user, $password, $database) {
	// use connect not pconnect for now, it's more solid. Avoids zombie connections.
	mysql_connect($host, $user, $password) or trigger_error("MySQL: Could not connect");
	mysql_select_db($database) or trigger_error("MySQL: Could not select DB");
	mysql_query("SET CHARACTER SET utf8");
}

function parse_mysql_timedate($datetime) {
	// takes mysql datetime and returns an array containing each element of the date

	$dt = explode(' ',$datetime);
	
	$tmp = explode('-',trim($dt[0])); 
	$ret['year'] = str_pad($tmp[0], 4, '200', STR_PAD_LEFT);
	$ret['month'] = str_pad($tmp[1], 2, '0', STR_PAD_LEFT);
	$ret['day'] = str_pad($tmp[2], 2, '0', STR_PAD_LEFT);

	if (isset($dt[1]) and !empty($dt[1])) {
		$tmp = explode(':',trim($dt[1]));
		$ret['hour']= str_pad($tmp[0], 2, '0', STR_PAD_LEFT);
		$ret['minute'] = str_pad($tmp[1], 2, '0', STR_PAD_LEFT);
		$ret['second'] = str_pad($tmp[2], 2, '0', STR_PAD_LEFT);
	}
	return ($ret);
}

function build_mysql_timedate($year, $month, $day, $hour="12", $minute="00", $second="00" ) {
	// more or less opposite of parse_mysql_timedate

	$day = str_pad($day, 2, '0', STR_PAD_LEFT);
	$month = str_pad($month, 2, '0', STR_PAD_LEFT);
	$year = str_pad($year, 4, '200', STR_PAD_LEFT);
	$hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
	$minute = str_pad($minute, 2, '0', STR_PAD_LEFT);
	$second = str_pad($second, 2, '0', STR_PAD_LEFT);
	return("$year-$month-$day $hour:$minute:$second");
}

class Record {
	// used for database records
	function Record($data="") {
		if (is_array($data)) {
			foreach($data as $item_key => $item) {
				$this->$item_key = $item;
			}
		}
	}
}

function sync_query($record, $table, $id_column = "id") {
	// like merge_query, but more performant and more elegant (produces one query)
	// update: multiple keys; $id_column as array
	$q_vals_set = $q_id_set = "";
	$cols = $vals = array();
	foreach($record as $key => $val) {
		if (is_string($val) or is_numeric($val) or is_bool($val) or is_null($val)) {
			if(is_null($val)) {
				$e_val = "NULL";
			} else if(is_numeric($val)) {
				$e_val = (int)$val;
			} else {
				$e_val = "'".mysql_real_escape_string($val)."'";
			}
			$e_key = mysql_real_escape_string($key);
			$is_id = (is_array($id_column) ? in_array($key, $id_column) : ($key == $id_column));
			$cols[] = "`$e_key`";
			$vals[] = $e_val;
			${( $is_id ? "q_id_set" : "q_vals_set")} .= " `$e_key` = $e_val,";
		}
	}
	$q = "
		INSERT INTO `".mysql_real_escape_string($table)."` (".implode(",",$cols).") 
		VALUES (".implode(",",$vals).")
		";
	($q_id_set.$q_vals_set != "") and $q .="ON DUPLICATE KEY UPDATE ".rtrim($q_id_set.$q_vals_set,", ");
	return($q);
}

function merge_query($record, $table, $id_column = "id") {
	// tries to provide the non implemented (in MySQL) SQL "MERGE" operation:
	// if row with given id already exists in table, update (on given columns only), otherwise insert.
	// can also be considered a smarter version of "insert_or_update_query()"
	// warning: not very performant, executes a query to find out if we should update or insert

	if ( isset($record->$id_column)) {
		$found_rows = mysql_num_rows(
			mysql_query(
				$q = "
					SELECT `$id_column` 
					FROM `$table` 
					WHERE `$id_column` = ".(is_numeric($record->$id_column)?(int)$record->$id_column:"'".$record->$id_column."'")
			)
		);
	} else {
		$found_rows = 0;
	}
	$found_rows > 1 and trigger_error("More then one supposedly unique row found.", E_USER_WARNING);
	return(insert_or_update_query($record, $table, (bool)($found_rows > 0), $id_column));
}

function insert_query($record, $table, $id_column = "id") {
	return(insert_or_update_query($record, $table, false, $id_column));
}

function update_query($record, $table, $id_column = "id") {
	return(insert_or_update_query($record, $table, true, $id_column));
}

function insert_or_update_query($record, $table, $update=null, $id_column = "id") {
	// generates an INSERT query for the $record object in the $table table
	// makes that an UPDATE query if $record contains a field named "id" (or $id_column if set)
	// $update can be set to force UPDATE or INSERT, otherwise it will be automatically determined (as above)
	// if hindered by the limitations of this function, try merge_query() instead

	$props = $propsNoId = "";
	$table = str_replace('`','',mysql_real_escape_string($table));
	$id_column = str_replace('`','',mysql_real_escape_string($id_column));	
	foreach($record as $propName => $propVal) {
		$propName = str_replace('`','',$propName);
		if (is_string($propVal) or is_numeric($propVal) or is_bool($propVal) or is_null($propVal)) {
			// skip arrays or objects
			if (is_null($propVal)) {
				$propValString = "NULL";
			} else {
				$propValString = "'".mysql_real_escape_string($propVal)."'";
			}
			$props .= "`$propName` = $propValString, ";
			($propName !== $id_column) and  $propsNoId .= "`$propName` = $propValString, ";
		}
	}
	$props = rtrim($props, ", ");
	$propsNoId = rtrim($propsNoId, ", ");	
	if (
			( isset($record->$id_column) and ($update !== false) )
			or ( $update === true )
		){
		$q = "
			UPDATE
			`$table`
			SET $propsNoId
			WHERE `$id_column` = ";
		$q .= is_numeric($record->$id_column) ? (int)$record->$id_column : "'" . mysql_real_escape_string($record->$id_column) . "'";		
	} else {
		$q = "
			INSERT INTO
			`$table`
			SET $props
		";
	}
	return($q);
}

function db_run($query) {
	// a wrapper for mysql_query that is smarter when it comes to (the lack of) error reporting
	!($res = mysql_query($query))
		and isset($_SERVER['DEVELOPMENT']) and $_SERVER['DEVELOPMENT'] 
		and trigger_error("Query failed: $query \r\n MySQL error: ".mysql_error())
	;
	return($res);
}

function readable_query($query) {
	// returns query with binary data and other huge fields reduced, for debug output and such
	// TBI
}

function get_rows_by_id_query ($table, $ids, $columns) {
	if (is_array($columns)) {
		$columns_safe = array();
		foreach($columns as $column) {
			$columns_safe[] = '`'.str_replace('`','',mysql_real_escape_string($column)).'`';
		}
		$column_q = implode(",",$columns_safe);
	} else {
		$columns_q = '`'.str_replace('`','',mysql_real_escape_string($columns)).'`';
	}
	if (is_array($ids)) {
		$ids_safe = array();
		foreach($ids as $id) {
			if (is_numeric($id)) {
				$ids_safe[] = (int)$id;
			} else {
				$ids_safe[] = "'".mysql_real_escape_string($id)."'";
			}
		}
		$ids_q = implode(",",$ids_safe);
	} else {
		$ids_q = mysql_real_escape_string($columns);
	}
	$table_safe = '`'.str_replace('`','',mysql_real_escape_string($table)).'`';
	return("SELECT $columns_q FROM $table_safe WHERE `id` IN ($ids_q)");
}

function getRows($query_or_result, $key_col = null) {
	/**
	* @return array|bool|void False if query fails or result resource is invalid, nothing (void) if invalid argument type given, array of rows (as objects) otherwise
	*/
	$items = array();
	is_string($query_or_result) and $res = mysql_query($query_or_result);
	is_resource($query_or_result) and $res = $query_or_result;
	if (isset($res)) {
		$res or trigger_error("Query: $query_or_result \n Yields:".mysql_error(), E_USER_WARNING);
		while($item = mysql_fetch_object($res)) {
			if (isset($item->$key_col) ) {
				$items[$item->$key_col] = $item;
			} else {
				$items[] = $item;
			}
		}
		return($res ? $items : $res);
	}
}

function getRow($query) {
	$tmp = getRows($query, 1);
	return(array_pop($tmp));
}

function instanciate($records, $class_name) {
	$objects = array();
	foreach($records as $record) {
		$objects[] = new $class_name($record);
	}
	return($objects);
}

function getColumn ($query, $value_property = null, $key_property = null) {
	// executes query and returns array with $key_property field as key and $value_property field as value
	// todo make method: $table->get_indexed_col($col, $index = "id")
	$array = array();
	$rows = getRows($query);
	if(is_null($value_property) and (count($rows)>0)) {
		$columns = array_keys(get_object_vars($rows[0]));
		$value_property = $columns[0];
	}
	foreach($rows as $rowkey => $row) {
		if (!is_null($key_property)) {
			$array[$row->$key_property] = $row->$value_property;
		} else {
			$array[$rowkey] = $row->$value_property;
		}
	}
	return($array);
}

function getCell ($query_or_result, $field = null) {
	$row = getRow($query_or_result);
	if(is_null($field) and is_object($row)) {
		$columns = array_keys(get_object_vars($row));
		$field = $columns[0];
	}
	if(isset($row->$field)) {
		return($row->$field);
	} else {
		return(null);
	}
}

function defineRelated ($related, $relationTable, $originId, $originCol, $destinationCol) {
	// inserts records in intermediate table for many-to-many relationships

	// weed out old relations
	mysql_query("DELETE FROM `$relationTable` WHERE `$originCol` = ".(int) $originId);
	$relations = "";
	foreach ($related as $related_id) {
		$relations .= "(".(int)$originId.",".(int)$related_id."),";
	}
	$relations = rtrim($relations,", ");
	if (!empty($relations)) {
		($res = mysql_query($relQ = "INSERT INTO `$relationTable` (`$originCol`, `$destinationCol`) VALUES $relations ")) or trigger_error("Query: $relQ failed with error: ".mysql_error());
		return($res);
	} else {
		return(false);
	}
}

function mres($string) {
	// just a wrapper because it gets tedious to write the full function name
	return(mysql_real_escape_string($string));
}


// =========================  mailing ============================== 


function sendContactMessage($data,$files=null,$from_address,$to_addresses,$from_name="",$subject="Web Form Submittal") {
	// $data = $_POST
	// $files = $_FILES

	$mail = new ezcMailComposer();
	$mail->from = new ezcMailAddress( $from_address, $from_name);	
	$mail->subject = $subject;
	if(is_array($to_addresses)) {
		foreach($to_addresses as $to_address => $to_name) {
			$mail->addTo( new ezcMailAddress( $to_address, $to_name ) );
		}
	} else {
		$mail->addTo( new ezcMailAddress( $to_addresses, "" ) );		
	}

	// prepare email body text
	$body = "\nYou have received a message through a web form ";
	$body .= "from IP address : ".$_SERVER['REMOTE_ADDR']."\n\n";

	foreach($data as $field => $val) {
		$body .= $field." : ".trim(stripslashes($val))."\n";
	}

	$mail->plainText = $body;

	// prepare attachments if need
	if(!is_null($files) and is_array($files) and !empty($files)) {
		foreach($files as $key => $val) {
			$mail->addStringAttachment($val['name'], file_get_contents($val['tmp_name']), $val['type']);
		}
	}

	$mail->build();
	// $host, [$user = ''], [$password = ''], [$port = null], [$options = array()] )
	$transport = new ezcMailSmtpTransport("localhost");

	// send email
	if (!isset($_SERVER['DEVELOPMENT'])) {
		$success = true;
		try {
			$transport->send($mail);
		} catch (Exception $e) {
			// throw $e;
			$success = false;
			$exception = $e->getMessage();
		}
	} else {
		// not a production environment, don't attempt to send
		sleep(1);
		$success = true; // set to true or false to control response in testing mode
	}
	$result = array(
		"success"=>$success,
	);
	isset($exception) and ($result['exception_message'] = $exception);
	return($result);
}

?>