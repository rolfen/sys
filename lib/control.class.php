<?

class Control {
	public $specs;  // array: specifications of the control
	public $name;	// unique (within the model) name of the control
	public $masterColumnName; // name of master column for this control in the record, usually same as $this->name
	public $record;	// the already existing record in the database we're working on, if any
	public $data; // data (state) of the control. Format is control-dependant.
	public function __construct($name, $specs) {
		$this->name = $this->masterColumnName = $name;
		$this->specs = $specs;
	}
	public function preload($record) { // retrieve state from record
		$this->record = $record;
	}
	public function put($data) { // receive new value from POST (or whatever)
		$this->data = $data;
	}
	public function dump() { // output array containing updated record field(s)
		return(array(
			$this->masterColumnName => $this->data
		));
	}
}

?>