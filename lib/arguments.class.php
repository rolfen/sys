<?

Class Arguments {
	public $arguments;
	public $keys;
	public $unparsed_keys;
	public $parsed_keys;
	function __construct($args = null) {
		$this->arguments = ( isset($args) ? $args : $_GET );
		$this->keys = $this->unparsed_keys = array_keys($this->arguments);
		$this->parsed_keys = array();
	}
	public function parse($key) {
		$value = $this->arguments[$key];
		$this->parsed_keys[] = $key;
		unset($this->unparsed_keys[$key]);
		return($value);
	}
	public function get($keys = null) {
		if(isset($keys) and is_array($keys)) {
			$arguments = array();
			foreach($keys as $key) {
				$arguments[$key] = $this->arguments[$key];
			}
			return($arguments);
		} else {
			return($this->arguments);
		}
	}
	public function keys_ending_with($str) {
		$found = array();
		foreach($this->keys as $key) {
			if(substr($key, 0, - count($key)) == $str) {
				$found[] = $key;
			}
		}
		return($found);
	}
}


?>