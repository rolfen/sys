<?


/*

Parses $_POST and $_FILE data (including file data) in an easy to use array or object
Mostly useful for access file data directly

$postData = new postData();
$post = $postData->get_all_as_array();

$nameString = $post->name;
isset($post->file) and $fileData = $post->file;  // that's it. No move_uploaded_file() etc.

*/

class PostData {
	var $post_data;
	var $file_data;

	var $errors;
	var $names;
	var $types;
	var $tmp_names;
	var $error_codes;
	var $sizes;

	var $errorsFulltext = array(
		UPLOAD_ERR_OK => 'There is no error, the file uploaded with success (UPLOAD_ERR_OK).',
		UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini (UPLOAD_ERR_INI_SIZE).',
		UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form (UPLOAD_ERR_FORM_SIZE).',
		UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded (UPLOAD_ERR_PARTIAL).',
		UPLOAD_ERR_NO_FILE => 'No file was uploaded (UPLOAD_ERR_NO_FILE).',
		UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder (UPLOAD_ERR_NO_TMP_DIR).',
		UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk (UPLOAD_ERR_CANT_WRITE).',
		UPLOAD_ERR_EXTENSION => 'A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help (UPLOAD_ERR_EXTENSION).'
	);

	function __construct() {
		$this->post_data = $this->file_data = array();
		$this->post_data = $_POST;
		isset($_FILES) and !empty($_FILES) and $this->load_files_array($_FILES);
	}

	function get_all_as_object() {
		return((object) $this->get_all_as_array());
	}

	function get_all_as_array() {
		return(array_merge_recursive($this->post_data, $this->file_data));
	}

	function load_files_array($files_array) {
		// normalizes and loads a $_FILES array
		foreach($files_array as $key => $val) {
			$this->names[$key] = $val['name']  ;
			$this->types[$key] = $val['type']  ;
			$this->tmp_names[$key] = $val['tmp_name']  ;
			$this->error_codes[$key] = $val['error']  ;
			$this->errors[$key] = $val['error'];
			$this->sizes[$key] = $val['size']  ;
		}
		$this->file_data = $this->load_file_data($this->tmp_names);
	}

	function load_file_data($tree) {
		// recursively traverses an md array and replace file paths with file data
		$loaded_data = array();
		foreach ($tree as $key => $val) {
			if (is_array($val)) {
				$loaded_data[$key] = $this->load_file_data($val);
			} else {
				is_file($val) and $loaded_data[$key] = file_get_contents($val);
			}
		}
		return($loaded_data);
	}

}


?>