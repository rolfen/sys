<? 

class DataObjectCollection {
	var $_db_table;
	var $_item_class;
	var $_item_ids; // array. if this is set, then collection will only contain these items, otherwise it will contain all items in the table

	// TODO:
	// get an array containing the first two child records using an array of IDs filter
	// $collection->filter(array(2,4,14))->get_records(2);
	// get an array of child objects using an SQL filter followed by an ID filter
	// $collection->filter("`place` LIKE 'Beirut'")->filter(1,2,3,4)->get_objects();
	// get only the first child object from the collection (will not be returned as array, since it's a single item)
	// $collection->get_object();
	// as oppposed to the current "apply_sql_filter" and "set_item_ids" implementations, the filters (filter()) should only be specific to every call
	// in other words, $coll->filter(1,2); $coll->get_objects(); is NOT equivalent to $coll->filter(1,2)->get_objects().
	// In the 1st form, the filter is not used and totally useless. The filter is only applied in the second form.
	// on the other hand: $filtered_coll = $coll->filter(1,2);$filtered_coll->get_objects(); is equivalent to the 2nd form.
	// PS: actually I prefer the filters to be applied on the object itself, this way we can instanciate on object per collection. It's a collection after all.
	// eg: $new_brands = new Brands();
	// $new_brands->apply_sql_filter("`is_new` = 1");
	// etc.
	// I'd like the function to be renamed to something starting with "select", to emphasize it's nature as a "selector", and to normalize with other future functions (eg "select_all", etc.)
	// maybe then we can also apply the "filter" thing above in parallel

	// TODO2: right now usually we have to do:
	// $brands = new Brands();
	// foreach($brands->get_all() as $brand) { ... }
	// would be nice to be able to do
	// $brands = new Brands();
	// foreach($brands as $brand) { ... }


	// this should replace pretty much all the get and filtering methods below!

	function set_item_ids($item_ids) {
		/**
		*  Load these ids in the collection.
		*/
		if (is_array($item_ids)) {
			$this->_item_ids = array();
			foreach ($item_ids as $item_id) {
				is_numeric($item_id) and $this->_item_ids[] = (int) $item_id;
			}
			// avoid SQL syntax errors if the list of ids is empty
			empty($this->_item_ids) and $this->_item_ids = array('NULL');
		} else {
			// setting anything else then an array effectively removes the filter
			$this->_item_ids = null;
		}
	}

	function select_with_related($relation) {
		$class_vars = get_class_vars($this->_item_class);
		if(isset($class_vars['_relations'][$relation])) {
			$relation_specs = $class_vars['_relations'][$relation];
			$relation_table = mysql_real_escape_string($relation_specs["relation_table"]);
			$relation_table_local_id = mysql_real_escape_string($relation_specs["relation_table_local_id"]);
			$q = "SELECT `id` FROM `".mysql_real_escape_string($this->_db_table)."` WHERE `id` IN (SELECT `".$relation_table_local_id."` FROM `".$relation_table."`)";
			isset($this->_item_ids) and $q .= " AND `id` IN (".implode($this->_item_ids,',').")";
			$this->set_item_ids(getColumn($q,null,'id'));
		} else {
			trigger_error("
				Relation '$related' is not defined.
				The following relations are defined for this object: ".implode(array_keys($class_vars['_relations']),", ")
			);
		}
	}

	function apply_sql_filter($sql) {
		/**
		* Reduces collection by an SQL "WHERE" clause, for example: $exampleCollection->apply_sql_filter("`title` LIKE '%snow%'") 
		* $sql MUST BE SAFE SQL!!!
		* with every filter application, $_item_ids is reduced. If you want to reset filters call set_item_ids() with a non-array argument (ex: null or "all")
		*/
		$q = "SELECT `id` FROM `".$this->_db_table."` WHERE ".$sql;
		isset($this->_item_ids) and $q .= " AND `id` IN (".implode($this->_item_ids,',').")";
		$this->set_item_ids(getColumn($q,null,'id'));
	}

	function __construct($item_ids=null) {
		isset($item_ids) and $this->set_item_ids($item_ids);
	}

	function save($dataObject) {
		// saves dataobject. Saves as new record if record does not contain an id property
		$record = new Record();
		$recordProperties = 0;
		if (method_exists($dataObject , "_save_pre")) {	
			$dataObject->_save_pre();
		}
		foreach ($dataObject as $dataObjectPropKey => $dataObjectPropVal) {
			if (substr($dataObjectPropKey,0,1) !== "_") {
				$recordProperties ++;
				$record->$dataObjectPropKey = $dataObjectPropVal;
			}
		}
		if ($recordProperties > 0) {
			$q = insert_or_update_query($record, $this->_db_table);
			if (mysql_query($q)) {
				isset($dataObject->id) or $dataObject->id = mysql_insert_id();
				$dataObject->save_related();
				if (method_exists($dataObject , "_save_post")) {	
					$dataObject->_save_post();
				}
				return(true);
			} else {
				trigger_error(mysql_error()." query: $q", E_USER_WARNING);
				return(false);
			}
		} else {
			trigger_error("Can't save nothing", E_USER_WARNING);
		}
	}
	function create($record=null) {
		// same as calling new on item class
		$newClassName = $this->_item_class;
		return(new $newClassName($record));
	}
	function get($id=null, $as_record=false) {
		// get $id as child object, or array of child objects if array of ids is given
		// if as_record is set to true, will return record objects instead of DataObjects
		// if id is null, gets first record from the list
		// TODO: reduce this code - lots of redundancy here
		if (is_scalar($id) and is_numeric($id) and ($id!="") ) {
			// this looks like a numerical ID
			$q = "SELECT * FROM `".$this->_db_table."` WHERE `id`=".(int)$id;
			isset($this->_item_ids) and $q .= " AND `id` IN (".implode($this->_item_ids,',').")";
			$record = getRow($q);
			if ($record and !empty($record)) {
				if ($as_record) {
					$item = $record;
				} else {
					$newClassName = $this->_item_class;
					$item = new $newClassName($record);
				}
				return($item);
			} else {
				return(false);
			}
		} else if (is_array($id) and (count($id) > 0)) {
			// array of IDs, I guess...
			$ids = "";
			$items = array();
			foreach($id as $single_id){
				$ids .= (int) $single_id . ", ";
			}
			$ids = rtrim($ids, ", ");
			$q = "SELECT * FROM `".$this->_db_table."` WHERE `id` IN ($ids)";
			isset($this->_item_ids) and $q .= " AND `id` IN (".implode($this->_item_ids,',').")";
			$itemRecords = getRows($q);
			$newClassName = $this->_item_class;
			$items = array();
			foreach ($itemRecords as $itemRecord) {
				if ($as_record) {
					$items[] = $record;
				} else {
					$items[] = new $newClassName($itemRecord);
				}
			}
			return($items);
		} else if (is_string($id) and trim($id)!="") {
			// A non-numerical and non-empty string; we assume $id is an SQL WHERE clause
			$q = "SELECT * FROM `".$this->_db_table."` WHERE ".$id;
			isset($this->_item_ids) and $q .= " AND `id` IN (".implode($this->_item_ids,',').")";
			$itemRecords = getRows($q);
			$newClassName = $this->_item_class;
			$items = array();
			foreach ($itemRecords as $itemRecord) {
				if ($as_record) {
					$items[] = $record;
				} else {
					$items[] = new $newClassName($itemRecord);
				}
			}
			return($items);
		}  else if(is_null($id)) {
			// get the first record
			$q = "SELECT * FROM `".$this->_db_table."` WHERE TRUE";
			isset($this->_item_ids) and $q .= " AND `id` IN (".implode($this->_item_ids,',').")";
			$record = getRow($q);
			if ($record and !empty($record)) {
				if ($as_record) {
					$item = $record;
				} else {
					$newClassName = $this->_item_class;
					$item = new $newClassName($record);
				}
				return($item);
			} else {
				return(false);
			}
		} else {
			return(false);
		} 
	}
	/* depriacated, implemented by get()
	// todo, merge get() get_one() and get_all() into get_single() and get_array() (returns all without argument) 
	function get_one($as_record = false) {
		// just get one record
		$q = "SELECT * FROM `".$this->_db_table."` WHERE TRUE ";
		isset($this->_item_ids) and $q .= " AND `id` IN (".implode($this->_item_ids,',').")";
		$q .= " LIMIT 1";
		$record = getRow($q);
		if ($record and !empty($record)) {
			if ($as_record) {
				$item = $record;
			} else {
				$newClassName = $this->_item_class;
				$item = new $newClassName($record);
			}
			return($item);
		} else {
			return(false);
		}
	}
	*/
	function get_all($fields = false, $as_records = false) {
		// $fields: array of columns to get. if associative array, it is "alias" => "sql expression", otherwise name of the column
		// get all child objects
		$items = array();
		if (is_array($fields)) {
			$fields_clause = "";
			foreach($fields as $fieldkey => $fieldname) {
				if (is_string($fieldkey) and !is_numeric($fieldkey)) {
					$fields_clause .= "$fieldname AS `".mysql_real_escape_string($fieldkey)."`,";
				} else {
					$fields_clause .= "`".mysql_real_escape_string($fieldname)."`,";					
				}
			}
			$fields_clause = rtrim($fields_clause, ",");
		} else {
			$fields_clause = "*";
		}
		$q = "SELECT $fields_clause FROM `".$this->_db_table."`";
		isset($this->_item_ids) and $q .= " WHERE `id` IN (".implode($this->_item_ids,',').")";
		$itemRecords = getRows($q);
		$newClassName = $this->_item_class;
		if ($as_records === false) {
			foreach($itemRecords as $itemRecord){
				$items[] = new $newClassName($itemRecord);
			}
		} else {
			$items = $itemRecords;
		}
		return($items);
	}
	// todo: get_where($sql_where_clause)
	function get_related($related) {

		// arg $related: string, name of the relation used
		// returns Array of IDs of records which are related to all items in the current collection
		// todo: make it accept objects too, like get_related in the DataObject class  

		$class_vars = get_class_vars($this->_item_class);
		if (isset($class_vars['_relations'][$related])) {
			$relation = $class_vars['_relations'][$related];
			$q =
			 	"SELECT `".$relation["relation_table_local_id"]."`,`".$relation["relation_table_foreign_id"].
				"` FROM `".$relation["relation_table"]."`"
			;
			isset($this->_item_ids) and $q .= " WHERE `".$relation["relation_table_local_id"]."` IN (".implode($this->_item_ids,',').")";
			return(getColumn($q,null,$relation["relation_table_foreign_id"]));
		} else {
			trigger_error("
				Relation '$related' is not defined.
				The following relations are defined for this object: ".implode(array_keys($class_vars['_relations']),", ")
			);
		}

		/*
		return(true);
		if (is_string($related)) {
			return(
				getColumn(
					"SELECT `".$relation["relation_table_local_id"]."`,`".$relation["relation_table_foreign_id"].
					"` FROM `".$relation["relation_table"].
					"` WHERE `".$relation["relation_table_local_id"]."` = ".(int)$this->id, null, $relation["relation_table_foreign_id"]
				)
			);
		} else if (is_subclass_of($related, "DataObjectCollection")) {
			// define relations between this data object and the given data object collection object
			$foreign_table = $related->_db_table;
			$related_ids = array();
			foreach($this->_relations as $relation_name => $relation_parameters) {
				if ($relation_parameters['foreign_table'] == $foreign_table) {
					$related_ids = array_merge($related_ids, $this->_related_records_ids[$relation_name]);
				}
			}
			$related_ids = array_unique($related_ids);
			return($related->get($related_ids));
		}
		*/
	}
	function count() {
		$q = "SELECT COUNT(`id`) as `count` FROM `".$this->_db_table."`";
		isset($this->_item_ids) and $q .= " WHERE `id` IN (".implode($this->_item_ids,',').")";
		$count = getRow($q);
		return((int) $count->count);
	}
	function get_chunk($chunk_num = 0, $chunk_size = 5) {
		// gets chunk of child records
		$items = array();
		$start_offset = $chunk_size * $chunk_num;
		$q = "SELECT * FROM `".$this->_db_table."`";  
		isset($this->_item_ids) and $q .= " WHERE `id` IN (".implode($this->_item_ids,',').")";
		$q .= " LIMIT ".(int)$start_offset.", ".(int)$chunk_size;
		$itemRecords = getRows($q);
		$newClassName = $this->_item_class;
		foreach($itemRecords as $itemRecord){
			$items[] = new $newClassName($itemRecord);
		}
		return($items);
	}
	function get_column($col) {
		// gets single column from collection, that is all values in that column, returns array indexed by id
		$q = "SELECT `id`, `$col` FROM `".$this->_db_table."`";
		isset($this->_item_ids) and $q .= " WHERE `id` IN (".implode($this->_item_ids,',').")";
		return(getColumn($q, "id", $col));
	}
	function delete($id) {
		$q = "DELETE FROM `".$this->_db_table."` WHERE `id` = ".(int)$id;
		isset($this->_item_ids) and $q .= " AND `id` IN (".implode($this->_item_ids,',').")";
		($ret = mysql_query($q) or trigger_error(mysql_error()));
		return($ret);
	}
}

?>