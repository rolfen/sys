
<?	


// todo: use Facebooks PHP "write HTML directly into PHP" thingie

function makeForm($dataFields, $data=null, $options=array() ) {
	/**
	*
	* @return string HTML form 
	* @param $dataFields An array describing the form structure. 
	* 	I found it best to describle the functionality by creating a (heavily commented) example $dataFields array which lists all the possible types of form rows that can be generated:
	* 
	*	$form_fields = array(
	*		"id" => array(
	*			"type" => "hidden"
	*		),
	*		"some_number" => array(
	*			"type"	=> "int"
	*		),
	*		"some_short_text_input" => array(
	*			"type" => "string"
	*		),
	*		"some_date" => array(
	*			"type" => "date"
	*		),
	*		"some_date_and_time" => array(
	*			"type" => "datetime"
	*		),
	*		"a_simple_textbox" => array(
	*			"type" => "text"
	*		),
	*		"a_rich_text_editor" => array(
	*			"type" => "richtext",								// a rich text editor
	*			"commands" => 	array(								// optional (overwrites defaults): array of richtext commands, in the form command_name (defined in javascript or in the browser) => command button label
	*				"bold" => "Bold",
	*				"link" => "Insert Link",
	*				"unlink" => "Remove Link",
	*			)
	*		),
	*		"a_file" => array(
	*			"type" => "file",											// file upload box
	*			"preview_url" => "binary.php?t=documents&c=pdf&id=412"		// optional: a small preview link will be shown
	*		),
	*		"an_image" => array(
	*			"type" => "image",											// image upload box
	*			"thumb_url" => "binary.php?t=images&c=img_thumb&id=12"		// Optional:URL of thumbnail to show as preview, under the file upload control
	*		),
	*		"a_select_dropdown" => array(
	*			"type" => "select",
	*			"attributes" => array(
	*				"onchange" => "someJavascript()"
	*			),
	*			"options" => array(									// the options displayed, with, for each, the array key (here 0,1 or 2) as "value" and array value as "label"
	*				0 => "Inactive",
	*				1 => "Active",
	*				2 => "Pending"
	*			)
	*		),
	*		"a_multiple_select_box" => array(
	*			"type" => "multiple",
	*			"options" => array(									// the options displayed, with, for each, the array key (here 0,1 or 2) as "value" and array value as "label"
	*				0 => "Inactive",
	*				1 => "Active",
	*				2 => "Pending"
	*			)
	*		),
	*		"a_multiple_select_box_with_grouping" => array(
	*			"type" => "multiple-grouped",						// multiple select, with grouped options
	*			"option_groups" => array(
	*				array(
	*					"label" => "Option Group 1",
	*					"options" => array(
	*						0 => "Inactive",
	*						1 => "Active",
	*						2 => "Pending"
	*					)
	*				),
	*				array(
	*					"label" => "Option Group 2",
	*					"options" => array(
	*						3 => "Some Option X",
	*						4 => "Some Option Y",
	*						5 => "Some Option Z"
	*					)
	*				)
	*			)
	*		),
	*		"an_image_cropping_box" => array(
	*			"type" => "cropbox",
	*			"crop_reference_url" => "binary.php?id=12&t=photos&c=crop reference",		// url of the image on which the cropping will happen
	*			"aspect_ratio" => "398:340",						// x:y
	*			"default" => array(									// Default crop coordinates... optional.... as an EXAMPLE of what coordinates there are
	*				"x1" => "crop_x1",
	*				"x2" => "crop_x2",
	*				"y1" => "crop_y1",
	*				"y2" => "crop_y2",
	*				"w" => "crop_w",
	*				"h" => "crop_h",
	*			)
	*		)
	*	);
	* 
	* Options common to all input types (some might not be implemented well accross all input types):
	* - label: The contents of the label for that input
	* - class: A CSS class to be applied to that input
	* - data: data for that input, if none is given, will load data from $data
	* - default: default data for that input if no data given. Set to null if you want the POST key not to exist at all.
	* - noload: do not load data for that input
	* - disabled: if set to true, input is "disabled" (read-only)
	* - datakey: name of the property in the data object from which to load data. Defaults to name of the key for this input in the $dataFields array.
	*
	* @param $data An object containing the data to be preloaded into the form. Every property of that object will be into the input defined in the key of the same name in the $dataFields array.
	* 
	*
	* @param $options An array, the form options, all optional:
	* - buttons (optional: deftaults to single submit buttons with label "Save"): 
	*	array(
	*		"type" =>  (N.I.!) string (optional, defaults to "submit"). "submit" | "action",N.I.: Not implemented yet! Maybe will never be because of limited use! Button is now usually a submit.
	*		"onclick"=> string (optional). some javascript.
	*		"confirm" => string (optional). If set, display a javascript confirm box upon form submission, and only continue if OK is clicked. Set this option to the message of the confirm box. 
	*		"label" => string (optional). The label of the button. Defaults to "Save" for buttons of type "submit"
	*		"submit_target" => array|string (optional, defaults to current $_GET URL). 
	*			Ex: if target is set to array("p"=>"salaries","sec"=>"employees"),
	*			then the form will be submitted to the URL ?p=salaries&sec=employees (for buttons of type "submit").
	*			The submit_target array will become the $_GET of the target script.
	*			Defaults to the current URL. Can also be an URL string.
	*	)
	*/
	$defaultRtCommands = array(	// default rich text commands (if no commands are defined in dataFields
		"bold" => "Bold",
		"italic" => "Italic",
		"underline" => "Underline",
		"link" => "Insert Link",
		"unlink" => "Remove Link",
		// "blockquote" => "Quote"
	);
	if(!isset($options['buttons'])) {
		$options['buttons'] = array(array()); // 1 button. Empty array will suffice (all values will be defaulted)
	}
	$confirm = isset($options['confirm']) ? $options['confirm'] : "" ;
	is_object($data) or $data = new stdClass();
	$form = $hiddenFields = "";
	$form .= "<form method=\"POST\" enctype=\"multipart/form-data\" autocomplete=\"off\" ";
	if(isset($options["submit_target"])) {
		if (is_array($options["submit_target"])) {
			$form .= 'action="?'.makeUrlQuery($options['submit_target']).'"';
		} else if (is_string($options["submit_target"])) {
			$form .= 'action="'.$options['submit_target'].'"';			
		}
	} 
	$form .= " >";
	$form .= "<table class=\"form\">";
    // debug($data);
	foreach ($dataFields as $fieldName => $fieldProps) {
		if(is_array($fieldProps) and !empty($fieldProps)) {
			$type = $fieldProps["type"];

			// load data from non default property?
			$datakey = isset($fieldProps["datakey"]) ? $fieldProps["datakey"] : $fieldName;
            // ($datakey != $fieldName) and debug("datakey for $fieldName changed to $datakey");

			// determine data for input
			unset($field_data); // we're in a foreach loop, unset it
			if ( isset($fieldProps['noload']) and $fieldProps['noload'] ) {
                // debug($fieldName.": noload is set");
				// bool flag noload: don't load data for this field. Unset because we're in a loop
				$field_data = "";
			} else if(isset($fieldProps["data"])) {
                // debug($fieldName.": data is set in form field");
				// mixed: data, overrides data from the $data object
				$field_data = $fieldProps["data"];
			} else if(isset($data->$datakey)) {
                // debug($fieldName.": data found in data array");
				$field_data = $data->$datakey;
            } else if(isset($fieldProps["default"])) {
                // debug($fieldName.": no data given but default is given");
				// default value for field if no data given
				$field_data = $fieldProps["default"];
			} else {
                // debug($fieldName.": no data given in any way - empty");
				// nothing found.
				$field_data = "";
			}

			// string additional classes / attributes for this input control
			$extra_attr = $class = "";
			if (isset($fieldProps["class"])) {
				$extra_attr .= ' class="'.$fieldProps["class"].'" ';
				$class .= $fieldProps["class"];
			}
			if (isset($fieldProps["attributes"]) and is_array($fieldProps["attributes"])) {
				foreach($fieldProps["attributes"] as $attr_key => $attr_val) {
					$extra_attr .= " $attr_key = \"$attr_val\" ";
				}
			}

			// bool input control disabled?
			if(isset($fieldProps["disabled"])) {
				$extra_attr .= ' disabled="disabled" ';
			}


			if (in_array($type, array("hidden","id"))) {
				// +++++++ hidden stuff
				$hiddenFields .= "\t\t<input type=\"hidden\" name=\"${fieldName}\" value=\"";
				isset($field_data) and $hiddenFields .= $field_data;
				$hiddenFields .= "\"/>\r";
			} else {
				// +++++++ rows
				$form .= "<tr>";
				$form .= "<td class=\"label\">";
				$form .= "<label>$fieldProps[label]</label>";
				// print rich text command buttons
				if ( in_array($type, array("richtext")) and !isset($fieldProps["disabled"])) {
					$commands = isset($fieldProps['commands']) ? $fieldProps['commands'] : $defaultRtCommands ;
					foreach ($commands as $command => $commandLabel) {
						$form .= "<Br/><button class=\"fancy-btn\" rt_command=\"$command\" >$commandLabel</button>";
					}
				}
				$form .= "</td>";
				$form .= "<td class=\"input\">";
				if (in_array($type, array("string","int"))) {
					$form .= "<input $extra_attr type=\"text\" name=\"${fieldName}\" value=\"$field_data\"/>";
				} else if (in_array($type, array("date","timedate","datetime"))) {
					switch($type) {
						case "datetime":
						case "timedate":
							$format = "%Y-%m-%d %H:%i:%S";
						break;
						default:
							$format = "%Y-%m-%d";
						break;						
					}
					$form .= "
						<div class=\"wrapper\">
							<input date $extra_attr type=\"text\" name=\"${fieldName}\" format=\"$format\" value=\"$field_data\"/>
							<a 
								class=\"fancy-btn inline\" 
								onclick='
									$(this).parent().children(\"input\").attr(\"value\",\"\");
								'
							>
							x</a>
						</div>
					";
				} else if (in_array($type, array("password"))) {
					$form .= "<input $extra_attr type=\"password\" name=\"${fieldName}\" value=\"$field_data\"/>";
				} else if (in_array($type, array("text"))) {
					$form .= "<textarea $extra_attr name=\"${fieldName}\">$field_data</textarea>";
				} else if (in_array($type, array("file","image"))) {
					if (in_array($type, array("file"))) {
						$input_html = "<input $extra_attr type=\"file\" name=\"${fieldName}\" />";
						if (isset($fieldProps["preview_url"]) and !empty($fieldProps["preview_url"])) {
							$input_html  .=  "<a href=\"".$fieldProps["preview_url"]."\" class=\"preview-link\" target=\"new\">Preview</a>";
						}
					} else if (in_array($type, array("image"))) {
						$input_html  = "<input $extra_attr type=\"file\" name=\"${fieldName}\" />";
						if (isset($fieldProps["thumb_url"]) and !empty($fieldProps["thumb_url"])) {
							$input_html  .=  "<img class=\"image-preview\" src=\"".$fieldProps["thumb_url"]."\">";
						} elseif (isset($fieldProps["preview_url"]) and !empty($fieldProps["preview_url"])) {
							$input_html  .=  "<img class=\"image-preview\" src=\"".$fieldProps["preview_url"]."\">";
						}
					}
					if(isset($fieldProps["clearable"])) {
						$form .= "
							<div class=\"wrapper\">
								$input_html
								<a 
									class=\"fancy-btn inline\" 
									onclick='
										$(this).parent().children(\"input\").prop(\"type\", \"hidden\");
										$(this).parent().children(\"input\").attr(\"value\",\"\");
										$(this).parent().append(\"<div>Will be deleted!</div>\");
										$(this).parent().children(\"div a\").remove();
									'
								>
								x</a>
							</div>
						";
					} else {
						$form .= $input_html;
					}
				}	else if (in_array($type, array("richtext"))) {
						$form .= "<div class=\"textarea ";
						isset($fieldProps["disabled"]) or $form .= "richtext";
						$form .= " $class\" name=\"${fieldName}\">".$field_data."</div>";
				}	else if (in_array($type, array("select","multiple"))) {
						$form .= "<select $extra_attr name=\"${fieldName}".(($type == "multiple")?"[]":"")."\" ".(($type == "multiple")?"multiple":"").">"; 
						// $form .= "<option value=''> -- Please Choose -- </option>";
						foreach($fieldProps['options'] as $optValue => $optLabel) {
							$form .= "<option value=\"$optValue\" ";
							if (
								isset($field_data)
								and (
									(is_scalar($field_data) and $field_data == $optValue)
									or (is_array($field_data) and in_array($optValue, $field_data))
								) 
							) {
								$form .= "selected";
							}
							$form .= ">$optLabel</option>";
						}	
						$form .= "</select>";
				}	else if (in_array($type, array("multiple-grouped"))) {
						$form .= "<select $extra_attr name=\"${fieldName}[]\" multiple>";
						foreach($fieldProps['option_groups'] as $optGroup){
							$form .= "<optgroup label=\"$optGroup[label]\">";
							foreach($optGroup['options'] as $optValue => $optLabel) {
								$form .= "<option value=\"$optValue\" ";
								if (
									isset($field_data)
									and (
										(is_scalar($field_data) and $field_data == $optValue)
										or (is_array($field_data) and in_array($optValue, $field_data))
									) 
								) {
									$form .= "selected";
								}
								$form .= ">$optLabel</option>";
							}
							$form .= "</optgroup>";
						}
					$form .= "</select>";
				}	else if (in_array($type, array("cropbox"))) {
					$form .= "<img class=\"cropbox\" src=\"$fieldProps[crop_reference_url]\" name=\"${fieldName}\" ";
					isset($fieldProps["aspect_ratio"]) and $form .= " aspect_ratio=\"$fieldProps[aspect_ratio]\" ";
					$form .= "/>";
    				(isset($field_data) and is_object($field_data)) and $field_data = (array) $field_data;
                    (isset($field_data) and is_array($field_data)) or $field_data = array();
					isset($field_data['x1']) or $field_data['x1'] = "";
					isset($field_data['y1']) or $field_data['y1'] = "";
					isset($field_data['x2']) or $field_data['x2'] = "";
					isset($field_data['y2']) or $field_data['y2'] = "";
					isset($field_data['w']) or $field_data['w'] = "";
					isset($field_data['h']) or $field_data['h'] = "";
					$hiddenFields .= "
					<input type=\"hidden\" cropbox_coord=\"x1\" name=\"${fieldName}[x1]\" value=\"".$field_data['x1']."\"></input>
					<input type=\"hidden\" cropbox_coord=\"y1\" name=\"${fieldName}[y1]\" value=\"".$field_data['y1']."\"></input>
					<input type=\"hidden\" cropbox_coord=\"x2\" name=\"${fieldName}[x2]\" value=\"".$field_data['x2']."\"></input>
					<input type=\"hidden\" cropbox_coord=\"y2\" name=\"${fieldName}[y2]\" value=\"".$field_data['y2']."\"></input>
					<input type=\"hidden\" cropbox_coord=\"w\" name=\"${fieldName}[w]\" value=\"".$field_data['w']."\"></input>
					<input type=\"hidden\" cropbox_coord=\"h\" name=\"${fieldName}[h]\" value=\"".$field_data['h']."\"></input>
					";
				}
				$form .= "</td>";
				$form .= "</tr>";
			} 
		} 		// end if is_array
	}  		// end main foreach
	if (isset($options['buttons']) and !empty($options['buttons'])) {
		$form .= "<tr>";
		$form .= "<td colspan=\"2\" class=\"buttons\">";
		foreach ($options['buttons'] as $button) {
			$form .= "<input type=\"submit\" value=\"".(isset($button['label'])?$button['label']:"Save")."\" ";
			if (isset($button['confirm']) or isset($button['onclick']) or isset($button['target'])) {
				$form .= "onClick=\"";
				if (isset($button['target'])) {
					if (is_array($button['target'])) {
						$form .= "this.form.action ='?".makeUrlQuery($button['target'])."'";
					} else if (is_string($button['target'])) {
						$form .= "this.form.action ='?".$button['target']."'; ";
					}
				}
				if (isset($button['onclick'])) {
					$form .= str_replace('"','\"',rtrim($button['onclick']," ;"))."; ";
				}
				if (isset($button['confirm'])) {
					$form .= "return(confirm('".htmlspecialchars($confirm)."'));";
				}
				$form .= "\" ";
			}
			$form .= ">";
		}
		$form .= "</td>";
		$form .= "</tr>";
	}
	$form .= "</table>";
	$form .= $hiddenFields;
	$form .= "</form>";
	return($form);
}


function makeGallery($specs, $data = array()) {
	/*
		see defaults below ($defaultSpecs)
		action / gallery_action format (array):
			target : string "popup"|"fancybox"|"fancypopup"|"overlay" (same shit). Null for regular html link.
			confirm : string. If set will display given string as confirmation message.
			handler : string. If given, html link target will begin with given_string.php
			variables : array. Array of key=>value to be encoded as URL arguments (ex ?id=2&page=4 etc.)
			label : string. Self explanatory.
		link_parameters is an array of key=>value to be encoded as URL arguments (ex ?id=2&page=4 etc.) for every thumbnail link

	*/
	$html = "<!-- makeGallery -->";
	$defaultSpecs = array( 
		"table" => "photos",
		"preview_column" => "thumb1",
		// todo replace "table" and "preview_column" by "thumb_parameters", modeled after "link_parameters", and add maybe "thumb_handler" option
		"id_key" => "id", // key name of id in data record
		// optional: "id_paremeter" : key name of id in url parameters. Overwrites that key in link_parameters. Defaults to id_key.
		"link_parameters" => array(),
		"label_key" => null, // if null, no label will be affixed to photo
		"actions" => array(),
		"sort_handler" => null,
		"gallery_actions" => array()
	);
	if (is_array($specs)) {
		$specs = array_merge($defaultSpecs, $specs);
		$table = $specs['table'];
		$previewColumn = $specs['preview_column'];
		$actions = $specs['actions'];
	} else {
		trigger_error("Argument must be an array.", E_USER_WARNING);
	}
	$html .= "<div class=\"gallery";
	isset($specs['sort_handler']) and $html .= " dragndrop";
	$html .= "\"";
	isset($specs['sort_handler']) and $html .= " sort_handler = \"".$specs['sort_handler']."\" ";
	$html .= "><!--\r";
	if(!empty($specs['gallery_actions'])) {
		$html .= "--><div class=\"gallery-box gallery-actions nodrag\">";
		// build action links
		foreach($specs['gallery_actions'] as $action => $actionVars) {
			if(isset($actionVars)) {
				// overwrite url parameters?
				$action_link_base = (isset($actionVars['link_parameters']) ? $actionVars['link_parameters'] : $specs["link_parameters"]);
				$classes = "fancy-btn ";
				if (isset($actionVars['handler'])) {
					// "hander" is defined, only put id in url, unless link_parameters are overwritten (that is defined in $actionVars)
					$actionLink = $actionVars['handler']."?".makeUrlQuery(isset($actionVars['link_parameters'])?$actionVars['link_parameters']:array());
				} else {
					$actionLink = "?".makeUrlQuery($action_link_base);
				}
				isset($actionVars['variables']) and !empty($actionVars['variables']) and $actionLink .= "&".makeUrlQuery($actionVars['variables']);
				$html .= "\t\t<a href=\"$actionLink\" ";
				if (isset($actionVars["confirm"])) { 
					$html .= "onClick=\"return(confirm('".escape_js_string($actionVars["confirm"])."'))\" ";
				}
				if (isset($actionVars["target"])) {
					switch ($actionVars["target"]) {
						case "popup":
						case "fancybox":
						case "fancypopup":
						case "overlay":
							$classes .= "fancypopup ";
						break;
						default:
							$html .= "target=".$actionVars["target"]." ";
						break;
					}
				}
				$html .= "class=\"$classes\" ";
				$html .= ">".$actionVars['label']."</a>\r";
			}
		}
		$html .= "</div><!--";
	}
	if (isset($data) and is_array($data)) {
		foreach($data as $key => $item) {
			$html .= "--><div class=\"gallery-box";
			isset($specs['sort_handler']) and $html .= " drag drop";
			$html .= "\"";
			isset($specs['id_key']) and !empty($specs['id_key']) and $html .= " data_id=\"".$item->$specs['id_key']."\"";
			$html .= ">\r";
			$html .= "<img class=\"thumb\" src=\"binary.php?t=${table}&c=${previewColumn}&id=".$item->id."\"/><br/>\r";
			$html .= "\t<div class=\"item-actions\">\r";
			// build action links
			foreach($actions as $action => $actionVars) {
				if(isset($actionVars)) {
					// manually define action url key in url?
					isset($actionVars["url_param"]) and $action = $actionVars["url_param"]; // deprecated alias
					isset($actionVars["id_parameter"]) and $action = $actionVars["id_parameter"];
					$action_id_value = ( isset($actionVars["id_key"]) ? $item->$actionVars['id_key'] : $item->$specs['id_key'] );
					// overwrite url parameters?
					$action_link_base = (isset($actionVars['link_parameters']) ? $actionVars['link_parameters'] : $specs["link_parameters"]);
					$classes = "fancy-btn ";
					if (isset($actionVars['handler'])) {
						// "hander" is defined, only put id in url, unless link_parameters are overwritten (that is defined in $actionVars)
						$actionLink = $actionVars['handler']."?".makeUrlQuery(array_merge((isset($actionVars['link_parameters'])?$actionVars['link_parameters']:array()), array($action=>$action_id_value)));
					} else {
						$actionLink = "?".makeUrlQuery(array_merge($action_link_base, array($action=>$action_id_value)));
					}
					isset($actionVars['variables']) and !empty($actionVars['variables']) and $actionLink .= "&".makeUrlQuery($actionVars['variables']);
					if (isset($specs['label_key']) and isset($item->$specs['label_key'])) {
						$html .= "<span class=\"label\">".$item->$specs['label_key']."</span>";
					}
					$html .= "\t\t<a href=\"$actionLink\" ";
					if (isset($actionVars["confirm"])) { 
						$html .= "onClick=\"return(confirm('".escape_js_string($actionVars["confirm"])."'))\" ";
					}
					if (isset($actionVars["target"])) {
						switch ($actionVars["target"]) {
							case "popup":
							case "fancybox":
							case "fancypopup":
							case "overlay":
								$classes .= "fancypopup ";
							break;
							default:
								echo "target=".$actionVars["target"]." ";
							break;
						}
					}
					$html .= "class=\"$classes\" ";
					$html .= ">".$actionVars['label']."</a>\r";
				}
			}
			$html .= "\t</div>\r";
			$html .= "</div><!--\r";
		}
	}
	$html .= "--></div>";
	return($html);

}


function makeList($listSpecs, $all_data=array()) {
	start_block($html);
	printList($listSpecs, $all_data);
	end_block($html);
	return($html);
}

function printList($listSpecs, $all_data=array()) { 

	!isset($listSpecs["link_parameters"]) and trigger_error('$listSpecs[\'link_parameters\'] is not set. It is a mandatory parameter. Check for typos.');

	$defaultListSpecs = array(
		"label_key" => "label",
		"link_parameters" => array(), // default link parameters, also used to determinte currently selected item in the list. must be passed parameters of current link
		"actions" => array(
			/*
			"delete" => array(
				"label" => "Delete",
				"confirm" => "Delete this?",
				"param_val" => 0, // opt: force this value on action. Otherwise, inherits value of id of list item,
				"label_key" => null // opt: if set, use value of this key in the data item instead of the static label
			)
			*/
		),
		"new_button" => true,		
		"id_key" => "id", // key associated with id in $all_data items	
		"id_parameter" => "id",	 // key associated with id in $listSpecs[link_parameters] array
		"page_parameter" => "page",	
		"items_per_page" => 30,		
		"order_key" => null,		
		"post_model" => null, 		// post_model need only to be set when order_key is set, as this will create a form
		"sort_handler" => null
	);
	if (is_array($listSpecs)) {
		$listSpecs = array_merge_recursive_simple($defaultListSpecs, $listSpecs);
	} else {
		trigger_error("Argument must be an array. Aborted.", E_USER_WARNING);
		return(false);
	}
	isset($listSpecs['id_parameter']) ?$curr_id = $listSpecs['link_parameters'][$listSpecs['id_parameter']] : $curr_id = "";

	// do pagination
	if (
			$pages = (ceil(count($all_data)/$listSpecs['items_per_page'])) 
			and $pages > 1 
			and isset($listSpecs['link_parameters'][$listSpecs['page_parameter']])
			and !empty($listSpecs['link_parameters'][$listSpecs['page_parameter']])
		) {
		$page = $listSpecs['link_parameters'][$listSpecs['page_parameter']];
		$data_chunks = array_chunk($all_data, $listSpecs['items_per_page']);
		$data = $data_chunks[$page-1];
		$pageLinks = "";
		for($i=1;$i<=$pages;$i++) {
			$pageLink = $listSpecs['link_parameters'];
			$pageLink[$listSpecs['page_parameter']] = $i;
			$pageLinks .= "<a ";
			($i == $page) and $pageLinks .= "selected ";
			$pageLinks .= "href=\"?".makeUrlQuery($pageLink)."\">$i</a>·";
		}
		$pageLinks = rtrim($pageLinks, '·');
	} else {
		$data = $all_data;
		unset($pages);	
	}

	// build the "new" link
	$newLinkBase = $listSpecs['link_parameters'];
	$newLinkBase[$listSpecs['id_parameter']] = "";
	$newLink = "?".makeUrlQuery($newLinkBase);

	// "linkBase"
	$linkBase = $listSpecs['link_parameters'];
	
?>
	<table class="rows" >
		<tbody 
			class="<?= isset($listSpecs['sort_handler'])?"dragndrop":""; ?>"
			<?= isset($listSpecs['sort_handler'])?"sort_handler = \"".$listSpecs['sort_handler']."\" ":"" ?>
		>
<? 		if (isset($pageLinks)) { ?>
		<tr>
			<td colspan="2">Page <?=$pageLinks?></td>
		</tr>
<? 		}		?>
<?		foreach($data as $iKey =>  $item) { 
			// build row specs for this item
			$linkSpecs = $listSpecs['link_parameters'];
			$linkSpecs[$listSpecs['id_parameter']] = $item->$listSpecs['id_key'];
			$rowLabel = ( !isset($item->$listSpecs['label_key']) or trim($item->$listSpecs['label_key']) == '' )?"[no label]":$item->$listSpecs['label_key'];
			$rowLabel = smart_truncate($rowLabel, 45);
			$link = "?".makeUrlQuery($linkSpecs);
?>
		<tr 
			class="<?= isset($listSpecs['sort_handler'])?"drag drop":""; ?>" 
			<? ( isset($curr_id) and ($curr_id != "") and ($curr_id == $item->$listSpecs['id_key'])) and print("selected") ?>	
			<? isset($listSpecs['id_key']) and !empty($listSpecs['id_key']) and print(" data_id=\"".$item->$listSpecs['id_key']."\" ") ?>
		>
		<td class="title">
			<a href="<?=$link?>"><?= $rowLabel ?></a>
		</td>
		<td class="actions">
<?
			// build action links
			foreach($listSpecs['actions'] as $action_key => $actionVars) {
				$action_key = isset($actionVars["url_param"]) ? $actionVars["url_param"] : $action_key; // deprecated, use id_parameter instead
				$action_key = isset($actionVars["id_parameter"]) ? $actionVars["id_parameter"] : $action_key;
				$action_id_value = ( isset($actionVars["id_key"]) ? $item->$actionVars['id_key'] : $item->$listSpecs['id_key'] );
				if(isset($actionVars["link_parameters"]) and is_array($actionVars["link_parameters"])) {
					// url parameters manually defined
					$actionParams = array_merge(
						$actionVars["link_parameters"],
						array(
							$action_key => $action_id_value
						)
					);
				} else {
					// either reset url parameters (new "handler") or inherit url parameters from list item
					$actionParams = array_merge(
						isset($actionVars['handler']) ? array() : $linkSpecs, 
						array(
							$action_key => $action_id_value
						)
					);
				}
				$actionLink = (isset($actionVars['handler']) ? $actionVars['handler'] : "")."?".makeUrlQuery($actionParams);
				echo "<a class=\"fancy-btn\" href=\"$actionLink\" ";
				if (isset($actionVars["confirm"])) { 
					echo "onClick=\"return(confirm('".escape_js_string($actionVars["confirm"])."'))\" ";
				}
				print(">".(
					(isset($actionVars['label_key']) and isset($item->$actionVars['label_key']))
					?$item->$actionVars['label_key']
					:$actionVars['label'] 
				)."</a>\r");

			}
?>
			<input type="hidden" variable="position" name="<?=$listSpecs['post_model']?>[<?=$item->$listSpecs['id_key']?>]" value="">
		</td>
	</tr>
<?		} ?>
	</tbody>
	<tfoot>
<? 		if ($listSpecs['new_button']) { ?>
	<tr class="nodrop nodrag" <? (isset($curr_id) and empty($curr_id)) and print("selected") ?>>
		<td class="title"></td>
		<td class="actions"><a class="fancy-btn" href="<?=$newLink?>">New</a></td>
	</tr>
<? 		} ?>
	</tfoot>
	</table>
<?	} ?>
