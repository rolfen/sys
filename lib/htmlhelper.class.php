<?

Class HtmlHelper {
	/* 

		Notes:

		The code for this class follows MS-style first-letter capitlization of variables and funcnames.

		Bugs:

		Seems to fall over with some versions of PHP when sys dir is not found - PHP just silently dies somewhere here
		these are the stacks on which behavior (presumably bug) has been observed:
		 - Apache/2.2.22 (Win32) mod_ssl/2.2.22 OpenSSL/0.9.8t PHP/5.3.25 (Visual C++ 2008 MSVC9)
	*/
	public function LinkScript($addr) {
		return("<script src=\"$addr\"></script>\n");
	}
	public function UTF8() {
		return("<meta charset=\"UTF-8\">");
	}
	public function Favicon($addr) {
		return("
			<link rel=\"icon\" 
      			type=\"image/png\" 
      			href=\"$addr\"
      		>
		");
	}

}

?>