<?

class IdControl extends Control {
	function put($data) {
		if(empty($data)) {
			$this->data = null;
		} else {
			$this->data = (int) $data;
		}
	}
	function dump() {
		if(is_null($this->data)) {
			return(array());
		} else {
			return(array(
				$this->masterColumnName => $this->data
			));
		}
	}
}


?>