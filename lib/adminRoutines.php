<?


// returns cropped image. if target_col is not null, also attempts to update database.
// Return cropped image, or false on failure.

function rebuildCroppedImage(
		$table,
		$id,
		$target_width = 300,
		$original_col = "img_original",
		$crop_reference_col = "img_crop_reference",
		$crop_coords_col = "img_crop_coords",
		$id_col = "id",
		$target_col = null
	) {
	$res = mysql_query($q1 = "
		SELECT 
			`".mysql_real_escape_string($original_col)."` AS `original`,
			`".mysql_real_escape_string($crop_reference_col)."` AS `crop_reference`,
			`".mysql_real_escape_string($crop_coords_col)."` AS `crop_coords`
		FROM
			".mysql_real_escape_string($table)."
		WHERE
			`".mysql_real_escape_string($id_col)."` = ".(is_numeric($id)?(int)$id:"'".mysql_real_escape_string($id)."'")."
	");
	$row = mysql_fetch_object($res);
	if (
        isset($row->crop_coords) and 
        $crop_coords = json_decode($row->crop_coords, true) and
        !empty($crop_coords["w"]) and
		!empty($crop_coords["h"]) and 
		!empty($crop_coords["x1"]) and 
		!empty($crop_coords["y1"]) and
		isset($row->original) and
		!empty($row->original) and
		isset($row->crop_reference) and
		!empty($row->crop_reference) and
		isset($row->crop_coords) and
		!empty($row->crop_coords)
	) {
		$scale = get_width($row->original) / get_width($row->crop_reference);
		$target_blob = crop(
			$row->original,
			$crop_coords["w"] * $scale ,
			$crop_coords["h"] * $scale ,
			$crop_coords["x1"] * $scale ,
			$crop_coords["y1"] * $scale ,
			( /* w in px: */ $target_width / ( $crop_coords["w"] * $scale ) )
		);
		if($target_blob) {
			if(!is_null($target_col) and !empty($target_col)) {
				mysql_query("
					UPDATE ".mysql_real_escape_string($table)." 
					SET `".mysql_real_escape_string($target_col)."` = '".mysql_real_escape_string($target_blob)."' 
					WHERE `".mysql_real_escape_string($id_col)."` = ".(is_numeric($id)?(int)$id:"'".mysql_real_escape_string($id)."'")."
				");
			}
			return($target_blob);
		} else {
			// crop failed
			return(false);
		}
	} else {
		// prerequisites not met
		return(false);
	}
}

function make_cropped_image (
	$img_original,
	$crop_coords,
	$img_crop_reference,
	$target_width
) {
// returns cropped (and scaled) image blob
	$scale = get_width($img_original) / get_width($img_crop_reference);
	$target_blob = crop(
		$img_original,
		$crop_coords["w"] * $scale ,
		$crop_coords["h"] * $scale ,
		$crop_coords["x1"] * $scale ,
		$crop_coords["y1"] * $scale ,
		( /* w in px: */ $target_width / ( $crop_coords["w"] * $scale ) )
	);
	return($target_blob);
}

function is_valid_crop_coords($crop_coords) {
	return(
		isset($crop_coords) and !is_null($crop_coords) and !empty($crop_coords)
		and isset($crop_coords["w"]) and !empty($crop_coords["w"])
		and isset($crop_coords["h"]) and !empty($crop_coords["h"])
		and isset($crop_coords["x1"]) and is_numeric($crop_coords["x1"])
		and isset($crop_coords["y1"]) and is_numeric($crop_coords["y1"])
	);
}

?>