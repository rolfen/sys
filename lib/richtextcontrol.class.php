<?

class RichtextControl extends Control {
	function __construct($name, $specs) {
		$this->name = $this->masterColumnName = $name;
		$this->specs = $specs;
	}
	function preload($record) {
		// retrieve state from record
		$this->record = $record;
	}
	function put($data) {
		// receive new value from POST (or whatever)
		$this->data = filter_html($data);
	}
	function __toString() {
		// when object is treated like a string
		return($this->data);
	}
}


?>