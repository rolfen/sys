<?

class DataObject {
	var $_relations = array();
	/*  Holds relation information. Example $_relations for a class:
	var $_relations = array(
		"books" =>  array(
			"foreign_table" => "book", (the table where the related records reside)
			"relation_type" => "many-to-many" (many-to-many|one-to-many|many-to-one)
			"relation_table" => "book2author", 
				(the table where the relation is defined - where the foreign index(es) is/are. in case of one-to-many it's the same table then the foreign-table, and in case of many-to-one it's the table that holds the record for this object)
			"relation_table_local_id" => "author_id", (the column that holds the id of the record assiciated with this object in the relation_table)
			"relation_table_foreign_id" => "book_id" (the column that holds the id of the related ("foreign") record in the relation_table)
		)
	);
	*/
	var $_related_records_ids = array();
	var $_related_loaded = false;
	/* $_related_records_ids is an array with one entry for each relation (defined in $_relation), each one of these contains an array of IDs. */
	function as_record() {
		/**
		* return object as record (basically same properties but without the hidden properties or any methods)
		*/
		$record = new Record();
		foreach ($this as $dataObjectPropKey => $dataObjectPropVal) {
			if (substr($dataObjectPropKey,0,1) !== "_") {
				$record->$dataObjectPropKey = $dataObjectPropVal;
			}
		}
		return($record);
	}
	function __construct($record=null) {
		/**
		* Constructor. Creates object from record
		*/
		if (!is_null($record)) {
			foreach($record as $itemColName => $itemColValue) {
				$this->$itemColName = $itemColValue;
			}
		}
	}
	function save_related() {
		/**
		* persist data stored in $this->_related_records_ids to database
		* TODO: remove the "defineRelated()" external dependency.
		* This is usually the only time this function is called, so we might as well move the code here, and eventually remove it from funcs.php
		*/
		$this->_related_loaded or $this->load_related();
		foreach ($this->_relations as $relation_name => $relation) {
			if($relation['relation_type']=="one-to-many" or $relation['relation_type']=="many-to-one") {

			} else if ($relation['relation_type']=="many-to-many") {
				if (isset($this->_related_records_ids[$relation_name]) and is_array($related = $this->_related_records_ids[$relation_name]) ) {
					defineRelated (
						$related,
						$relation['relation_table'],
						$this->id,
						$relation["relation_table_local_id"],
						$relation["relation_table_foreign_id"]
					);
				}				
			}
		}
	}
	function load_related() {
		/**
		* gets related ids for each relation from related table and loads them in to the $this->_related_records_ids array
		*/
		if (isset($this->id) and !empty($this->id)) {
			foreach($this->_relations as $relation_name => $relation) {
				$related_ids = getColumn("SELECT `".$relation["relation_table_local_id"]."`,`".$relation["relation_table_foreign_id"]."` FROM `".$relation["relation_table"]."` WHERE `".$relation["relation_table_local_id"]."` = ".(int)$this->id, null, $relation["relation_table_foreign_id"]);
				$this->_related_records_ids[$relation_name] = $related_ids;
			}
		}
		$this->_related_loaded = true;
	}
	function set_related($relation, $ids) {
		/**
		* set ids (array) as the related objects for relation $relation
		*/
		$this->_related_loaded or $this->load_related();
		if (isset($this->_relations[$relation])) {
			$this->_related_records_ids[$relation] = $ids;
		} else {
			trigger_error("Relation \"$relation\" does not exist", E_USER_WARNING);
		}
	}
	function add_related($relation, $id) {
		/**
		* adds id of a related object for relation $relation
		* TODO: add_related($related, $relation), with $related possibly being an id, an array of ids, a related object, or array (or collection?) or related objects
		*/
		$this->_related_loaded or $this->load_related();
		if (isset($this->_relations[$relation])) {
			if (is_array($id)) {
				foreach ($id as $single_id) {
					$this->_related_records_ids[$relation][] = $single_id;
				}
			} else {
				$this->_related_records_ids[$relation][] = $id;
			}
		} else {
			trigger_error("Relation \"$relation\" does not exist", E_USER_WARNING);
		}
	}
	function remove_related($relation, $id) {
		/**
		* removes an id from the array of related records ids for $relation
		*/
		$this->_related_loaded or $this->load_related();
		if (isset($this->_relations[$relation])) {
			$related_ids = $this->_related_records_ids[$relation];
			unset($this->_related_records_ids[$relation][array_search($id,$related_ids)]);
		} else {
			trigger_error("Relation \"$relation\" does not exist", E_USER_WARNING);
		}
	}
	function get_related($related) {
		/*
		* @return Array of related IDs if $related is a string, array of related DataObject objects if $related is a DataObjectCollection object.
		*/
		$this->_related_loaded or $this->load_related();
		if (is_string($related)) {
			return($this->_related_records_ids[$related]);
		} else if (is_subclass_of($related, "DataObjectCollection")) {
			// define relations between this data object and the given data object collection object
			$related_ids = array();
			foreach($this->_relations as $relation_name => $relation_parameters) {
				if ($relation_parameters['foreign_table'] == $related->_db_table) {
					$related_ids = array_merge($related_ids, $this->_related_records_ids[$relation_name]);
				}
			}
			$related_ids = array_unique($related_ids);
			return($related->get($related_ids));
		} else {
			trigger_error("Unsupported data type / class for argument", E_USER_WARNING);
		}
	}
}

?>