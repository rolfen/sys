-- The user table for CMS login

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `lastfailedattempt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- This would create a user admin with password admin (using SHA1())
-- INSERT INTO `user` (`id`, `username`, `password`, `lastfailedattempt`) VALUES
--	(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 0);