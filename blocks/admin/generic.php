<?
require_once ("app.php");
require_once (SYSROOT."login.php");
require_once (SYSROOT."lib/filterHtml.php");
require_once (SYSROOT."lib/adminHtml.php");
require_once (SYSROOT."lib/adminRoutines.php");

/*
	the structure of the specs array is still somehow unfixed, 
	so avoid referencing it directly. Create variables instead.
	Plus it makes the code more readable.
*/

$languageId = ( isset($_SESSION['language']) ? $_SESSION['language'] : 1 );

function array_extract($array, $keys) {
	return(array_intersect_key($array, array_fill_keys($keys, true)));
}

class ModelGetter {
	function columns($modelControlSpecs, $table) {
		$ret = array();
		foreach($modelControlSpecs as $controlName => $controlSpecs) {
			(isset($controlSpecs['multilingual']) and $controlSpecs['multilingual']) ?
				$ret["${table}_translation"][] = $controlName
				: $ret["${table}_translation"][] = $controlName;
		}
		return($ret);
	}
	function sqlSelectors($modelControlSpecs, $table) {
		$dataColumnSqlSelectors = array();
		foreach($modelControlSpecs as $controlName => $controlSpecs) {
			(isset($controlSpecs['multilingual']) and $controlSpecs['multilingual']) ?
				$dataColumnSqlSelectors[] = "`${table}_translation`.`${controlName}`"
				: $dataColumnSqlSelectors[] = "`${table}`.`${controlName}`" ;
		}
		return(implode(',',$dataColumnSqlSelectors));
	}
	function tableHasTranslation($modelControlSpecs) {
		$tableHasTranslation = false;
		foreach($modelControlSpecs as $controlName => $controlSpecs) {
			isset($controlSpecs['multilingual']) and $controlSpecs['multilingual'] and ($tableHasTranslation = true);
		}
		// print_r($modelControlSpecs);
		return($tableHasTranslation);
	}
	function buildQuery($modelControlSpecs, $table, $id = null, $languageId = 1, $keys = null) {
		$q = "SELECT ".self::sqlSelectors($modelControlSpecs, $table)." FROM `$table`";
		self::tableHasTranslation($modelControlSpecs) and $q .= "LEFT JOIN `".$table."_translation` ON 
				`${table}`.id = `${table}_translation`.`${table}_id` AND 
				`${table}_translation`.`language_id` = ".(int)$languageId;
		$q .= " WHERE TRUE ";
		isset($id) and $q .= " AND `$table`.`id` = ".(int)$id;
		is_array($keys) or $keys = array();
		foreach($keys as $k => $v) {
			$q .= " AND `$k` = ".(int)$v;
		}
		return($q);
	}
}

$table = $model = $Args->parse('model');

if(isset($config['models'][$model])) {
	$specs = $config['models'][$model];
/*
} else if($modelPath = explode('_', $model) and is_array($modelPath) and (count($modelPath) > 1)) {
	$parentModelName = $modelPath[0];
	$specs = $config['models'][$parentModelName]['models'][$modelPath[1]];
*/
} else {
	// only one level of recursion supported. Todo: make fully recursive.
	foreach($config['models'] as $modelName => $modelSpecs) {
		if(isset($modelSpecs['models'][$model])) {
			$parentModelName = $modelName;
			$parentModelSpecs = $modelSpecs;
			$specs = $modelSpecs['models'][$model];
			//$translated = isset($specs['multilingual']) or isset($specs['translated']);
			break;
		}
	}
	if(!isset($specs)) {
		trigger_error("no model definition found for $model");	
	}
}


// actions

if(isset($Args->arguments['action']) and $action = $Args->parse('action')) {
	switch($action) {
		case "delete":
			db_run($q = "DELETE FROM `".mysql_real_escape_string($_GET['action_target_table'])."` WHERE `id` = ".(int) $_GET['action_target_id']);
		break;
	}
}

// controls or "master columns"

$controls = array_merge_recursive(
	array(
		'id' => array(
			'type' => 'id',
			'default' => null
		),
	),
	$specs['controls'] 
);

isset($languageId) and ModelGetter::tableHasTranslation($controls)
and $controls = array_merge_recursive(
	array(
		'language_id' => array(
			'type' => 'hidden',
			'multilingual' => true,
			'default' => $languageId
		),
	),
	$controls 
);

if(isset($parentModelName) and isset($_GET[$parentModelName.'_id'])) {
	$controls = array_merge_recursive(
		array(
			$parentModelName.'_id' => array(
				'type' => 'hidden',
				'default' => $_GET[$parentModelName.'_id']
			),
		),
		$controls 
	);
}


$specs['controls']  = $controls;

// $Args->get($Args->keys_ending_with("_id"));

// columns we want to pull data from in the db. 

$columns = $controlObjects = array(); 

foreach($controls as $controlKey => $control) {
	$controlClassName = ucfirst($control['type']).'Control';
	if(class_exists($controlClassName)) {
		$controlObjects[$controlKey] = new $controlClassName($controlKey, $control);
	} else {
		$controlObjects[$controlKey] = new Control($controlKey, $control);
	}
	switch ($control['type']) {
		case 'image':

		break;		
		default:
			$columns[$controlKey] = $control;
		break;
	}
}



// if we receive POST, process it
if(isset($_POST) and !empty($_POST)) {
	$postObj = new PostData();
	$post = $postObj->get_all_as_array();
	$record = new Record();
	foreach($post as $postKey => $postVal) {
		if(
			isset($controls[$postKey]) // we have a definition for this key
		) {
			if(isset($controlObjects[$postKey])) {
				// there is a class defined for this control, use it
				$controlObject = $controlObjects[$postKey];
				$controlObject->put($postVal);
				$record = copy_properties($controlObject->dump(), $record);
			} else if($postKey != 'id' or !empty($postVal)) {
				// defaut: just pass on to query (assume the post key = sql column name)
				$record->$postKey = $postVal;
			}
		}
	}
	if(!empty($record)) {
		// move translated controls to a secondary query
		isset($_POST['language_id']) and $translation_record = new Record();
		foreach ($record as $k => $v) {
			if(isset($translation_record) and isset($controls[$k]['multilingual']) and $controls[$k]['multilingual']) {
				$translation_record->$k = $record->$k;
				unset($record->$k);
			}
		}
		db_run(sync_query($record,$table));
		$record_id = (isset($record->id) ? $record->id : mysql_insert_id());
		!empty($translation_record) 
			and $record_id
			and (($translation_record->language_id = $_POST['language_id']) or true)
			and (($translation_record->{$table."_id"} = $record_id) or true)
			and db_run(sync_query($translation_record,$table."_translation",array($table."_id","language_id")))
		;
	}
}


// preload data for edited item (if any)
if (isset($_GET['id']) and !empty($_GET['id'])) {
	$q = ModelGetter::buildQuery($controls,$table,$_GET['id'],$languageId);
	$item = getRow($q);
} else {
	$item = null;
}

// print everything
if(!isset($_GET['minimal'])) {
	$COL[0] .= makeListing(
		$specs
		,$model
		,isset($parentModelSpecs) ?$parentModelSpecs :null
		,isset($parentModelName) ?$parentModelName :null
		,isset($languageId) ?$languageId :null
	);
	$COL[1] = makeForm($controls, $item);
} else {
	$COL[0] = makeForm($controls, $item);
}

// -------------- functions below


function makeListing($modelSpecs, $modelName, $parentModelSpecs = null, $parentModelName = null, $languageId = null) {

	// the listed table

	// global $Args
	// $filters = $Args->get($Args->keys_ending_with("_id"));

	if(isset($modelSpecs['table']) and !empty($modelSpecs['table'])) {
		$table = $modelSpecs['table'];
	// } else if(isset($parentModelName)) {
	// 	$table = $parentModelName.'_'.$modelName;
	} else {
		$table = $modelName;
	}
	$template = isset( $modelSpecs['list_template'] ) ? $modelSpecs['list_template'] : 'list';
	switch($template) {
		case "gallery":
			$dataColumns = array('id');
			$generatorFunc = 'makeGallery';
			$templateSpecs = array(
				'table' => $table,
				'preview_column' => 'img_thumb',
				'gallery_actions' => array(
					'new' => array(
						'label' => "New",
						'target' => 'popup',
						"link_parameters" => array(
							"model" => $table,
							"minimal" => 1
						)
					)
				),
				'actions' => array(
					'edit' => array(
						"id_parameter" => "id",
						"label" => "Edit",
						"target" => "popup",
						"link_parameters" => array(
							"model" => $table,
							"minimal" => 1
						)
					),
					'delete' => array(
						"id_parameter" => "action_target_id",
						"label" => "x",
						"link_parameters" => array(
							"model" => $_GET['model'],
							"id" =>  $_GET['id'],
							"action_target_table" =>  $table,
							"action" => "delete",
						),
						"confirm" => "Are you sure you want to delete this item?"
					),
				)
			);
			if(isset($parentModelName)) {
				$templateSpecs['gallery_actions']['new']['link_parameters'][$parentModelName.'_id'] = $_GET[$parentModelName.'_id'];
				$templateSpecs['actions']['edit']['link_parameters'][$parentModelName.'_id'] = $_GET[$parentModelName.'_id'];
			}
		break;
		default:
		case 'list':
			$labelKey = isset($modelSpecs['label_key']) ? $modelSpecs['label_key'] : 'id';
			$generatorFunc = 'makeList';
			$dataColumns = array('id');
			$labelKey !== 'id' and $dataColumns[] = $labelKey;
			$templateSpecs = array(
				'link_parameters' => array(
					"model" => $modelName,
					"id" => isset($_GET['id']) ? $_GET['id'] : '',
				),
				'label_key' => $labelKey,
				'actions' => array(
					'delete' => array(
						"id_parameter" => "action_target_id",
						"label" => "x",
						"link_parameters" => array_merge( $_GET, array(
							"model" => $_GET['model'],
							"id" =>  $_GET['id'],
							"action_target_table" =>  $table,
							"action" => "delete",
						)),
						"confirm" => "Are you sure you want to delete this item?"
					),
				), 
			);
			if(isset($modelSpecs['models'])) {
				$subModels = $modelSpecs['models'];
				foreach($subModels as $subModelName => $subModel) {
					$templateSpecs['actions'][$subModelName] = array(
						"id_parameter" => $modelName."_id",
						"link_parameters" => array(
							"id" => "",
							"model" => $subModelName,
							$modelName."_id" =>  $_GET['id'],
						),"label" => (isset($subModel['label']) ? $subModel['label'] : $subModelName )
					);
				}
			}
			if(isset($parentModelName)) {
				$templateSpecs['link_parameters'][$parentModelName.'_id'] = $_GET[$parentModelName.'_id'];
			}
		break;
	}
	if(isset($templateSpecs)) {
		$parentModelKeys = (isset($parentModelName) and isset($_GET[$parentModelName.'_id']))? array($parentModelName.'_id' => $_GET[$parentModelName.'_id']) : null;
		$q = ModelGetter::buildQuery($modelSpecs['controls'],$table,null,$languageId,$parentModelKeys);
		$dataRows = getRows($q);
		$out = $title = "";
		$modelLabel = ( isset($modelSpecs['label']) ?$modelSpecs['label'] :$modelName );
		if(isset($parentModelName)) {
			$parentRecordLabel = getCell(ModelGetter::buildQuery(array_extract($parentModelSpecs['controls'],array($parentModelSpecs['label_key'])),$parentModelName,$_GET[$parentModelName.'_id']));
			$title = "<h3>$modelLabel for <a href=\"?model=$parentModelName&id=".(int)$_GET[$parentModelName.'_id']."\">$parentRecordLabel</a></h3>";
		} else {
			$title = "<h3>$modelLabel</h3>";
		}
		$out .= $title . $generatorFunc($templateSpecs,$dataRows);
		return($out);
	} else {
		trigger_error("English, please");
	}
}


?>