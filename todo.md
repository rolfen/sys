
ToDo
===========

* Document
* Rewrite DB layer
* Tidy up unused code.
* Get rid of unmaintained dependencies.
* Add examples
* Resonsive admin!
* Decouple HTML from PHP in admin form generation
* Break down into modular structure, without adding complexity. In other words, refactor.
* Audit security