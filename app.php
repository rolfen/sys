<?

/* 
	This file sets up the environment 
	It should be included early (first or something)
*/


// USR (user) and SYSROOT indicate the user and sys root directories, 
// used to build paths to resources
defined('SYSROOT') or define('SYSROOT', './');
defined('USRROOT') or define('USRROOT', SYSROOT);

include_once(SYSROOT.'lib/funcs.php');

// Connection settings. Don't forget to connect!

// mysql_connect('127.0.0.1', 'sys_example', 'ButterPassword');
// mysql_select_db('sys_example_gallery');


function autoload($class_name) {
	// find and include class files automatically
	if (is_file(USRROOT."lib/".strtolower($class_name).".class.php")) {
		$include = USRROOT."lib/".strtolower($class_name).".class.php";
	} else if(is_file(SYSROOT."lib/".strtolower($class_name).".class.php")) {
		$include = SYSROOT."lib/".strtolower($class_name).".class.php";
	}
	if(isset($include)) {
		require_once($include);
	} else {
		require_once SYSROOT."lib/ezc/Base/src/base.php";
		ezcBase::autoload( $class_name );
	} 
}

spl_autoload_register("autoload");

// init globals
if (!isset($config)) {
	$config = array();
}

?>