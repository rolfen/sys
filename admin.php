<?
/*

$_GET['minimal'] == true => Print a "minimal version" which is a page with only the contents, no menu and stuff

*/
// includes and global vars
require_once ("app.php");
require_once (SYSROOT."login.php");
require_once (SYSROOT."lib/filterHtml.php");
require_once (SYSROOT."lib/adminHtml.php");
require_once (SYSROOT."lib/adminRoutines.php");

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- force standards mode when available -->
	<link rel="stylesheet" href="<?= SYSROOT ?>css/admin.css" type="text/css">
	<link rel="shortcut icon" href="<? USRROOT ?>favicon.ico">

	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- force standards mode when available -->

	<script type="text/javascript" src="<?= SYSROOT ?>js/jquery.min.js"></script>
	<script type="text/javascript" src="<?= SYSROOT ?>js/jquery-ui-custom.min.js"></script>


	<script src="<?= SYSROOT ?>js/rangy-core.js"></script>
	<script src="<?= SYSROOT ?>js/rangy-cssclassapplier.js"></script>

	<link rel="stylesheet" href="<?= SYSROOT ?>css/admin.css" type="text/css">
	<link rel="stylesheet" href="<?= SYSROOT ?>css/imgareaselect-animated.css" type="text/css">
	<link rel="stylesheet" href="<?= SYSROOT ?>css/anytime.compressed.css" type="text/css">
	<link rel="stylesheet" href="<?= SYSROOT ?>css/jquery.fancybox.css" type="text/css">
	<link rel="stylesheet" href="<?= SYSROOT ?>css/chosen.css" type="text/css">


	<script type="text/javascript" src="<?= SYSROOT ?>js/jquery.imgareaselect.pack.js"></script>
	<script type="text/javascript" src="<?= SYSROOT ?>js/anytime.compressed.js"></script>
	<script type="text/javascript" src="<?= SYSROOT ?>js/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="<?= SYSROOT ?>js/jquery.chosen.min.js"></script>


	<script type="text/javascript" src="<?= SYSROOT ?>js/admin.js"></script>


	<title><?=$config["site_name"] ?> Website Administration</title>
</head>
<body>
	<? if(!isset($_GET['minimal'])) { ?>
	<div id="header">
		<img id="logo" src="<?= USRROOT ?>/img/logo.png" />
		<div id="buttons">
			<? foreach($languages as $k => $lang) { ?>
				<a class="button <?= (( isset($_SESSION['language']) and ($_SESSION['language'] == $lang->id)) ? 'selected' : '') ?>" href="?<?= makeUrlQuery(array_merge($_GET,array('setlang'=>$lang->id))) ?>"><?= $lang->name ?></a>
			<? } ?>
			<span style="float:right; color:white; margin-left:5px;"> | </span>
			<a class="button" href="?logout">Log Off</a>
			<? foreach($config['models'] as $model => $specs) {  if(!isset($specs['is_master']) or $specs['is_master'])  { ?>
			<a class="button <?= ( ($model==$_GET['model']) or ($model==substr($_GET['model'],0,strlen($model))) )  ?"selected":""?>" href="?model=<?= $model ?>&id="><?= isset($specs['label']) ? $specs['label'] : ucfirst($model) ?></a>
			<? }} ?>
			<a class="button <?=$p=="home"?"selected":""?>" href="?p=home">Home</a>
		</div>
	</div>
	<? } ?>
	<? if (isset($MESSAGE) and !empty($MESSAGE)) { ?>
	<div class="message">
		<? isset($MESSAGE) and !empty($MESSAGE) and print($MESSAGE) ?>		
	</div>
	<? } ?>
	<table>
		<tr>
			<td id="main">
				<?=@$COL[0]?>
			</td>
			<? if(!isset($_GET['minimal'])) { ?>
			<td id="right">
				<?=@$COL[1]?>
			</td>
			<td id="right2">
				<?=@$COL[2]?>
			</td>
			<? } ?>
		</tr>
	</table>
</body>
</html>