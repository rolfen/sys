<?

/*
 
 This file handles login. If you include it it will require presence of a session with $_SESSION['logged_in'] being true, otherwise it will print a login page and halt script execution (die()).

 If it finds all of the the following globals defined:
  - $_POST['username']
  - $_POST['password']
  - $_POST['model'] (set to "login")
  then it will try to authenticate against the `user` table in the database, and start an authenticated session (that is set $_SESSION['logged_in']), and, on success, give back control to the including file, without printing the login form.

 Include this file at the beginning of your script, before sending any HTML.
 Include for all authenticated pages.

 */

header('Strict-Transport-Security: max-age=0');

require_once("app.php");

(session_id() == "") and session_start();

if ( isset($_GET['logout']) ) {
	// logout requested
	unset($_SESSION['admin_logged_in'], $_SESSION['username'], $_SESSION['start']);
	// session_destroy();	
	header("Location: ./");
	die();
}

if (isset($_POST['model']) and ($_POST['model'] == "login") and isset($_POST['username']) and isset($_POST['password'])) {
	// attempt to authenticate
	$username = mysql_real_escape_string($_POST['username']);
	$password = mysql_real_escape_string($_POST['password']);
	$q = "SELECT * FROM `user` WHERE `username` = '$username' ";
	$res = mysql_query($q);
	$user = mysql_fetch_object($res);
	mysql_free_result($res);
	if (is_object($user)) {
		if ( (time() - $user->lastfailedattempt) > 10) {
			if ( $user->username == $username and $user->password == sha1($password) ) {
				$_SESSION['admin_logged_in'] = true;
				$_SESSION['username'] = $username;
				$_SESSION['start'] = $_SESSION['last_action'] = time();
			} else {
				mysql_query("UPDATE `user` SET `lastfailedattempt` = ".time()." WHERE `username` = '".$username."'");
				$message = "Sorry, login failed! You have to wait 10 seconds before trying again for this user.";
			}
		} else {
			$message = "Please wait 10 seconds between failed login attempts.";
		}
	} else {
		$message = "This user does not exist or is not allowed to login.";
	}
}

if (isset($_SESSION['admin_logged_in']) and ($_SESSION['admin_logged_in'] === true) and isset($_SESSION['last_action']) ) {
	// session active
	// check for session timeout

	// session timeout period in seconds		
	$inactive = 20 * 60; // minutes before timeout
	$session_life = time() - $_SESSION['last_action'];

	if ( 
		// we don't time out in development environment
		!(isset($_SERVER['DEVELOPMENT']) and $_SERVER['DEVELOPMENT'])
		and 
		($session_life > $inactive)
	) {
		// session timed out
		unset($_SESSION['admin_logged_in'], $_SESSION['username'], $_SESSION['start']);
		// session_destroy();
		$message = "Your session has timed out. Please log back in.";
	} else {
		// all checks passed. renturn to calling script.
		$_SESSION['last_action'] = time();
		return("logged_in");
	}
}

// if we got so far, then user not logged in. print login form below then die.

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /> <!-- IE 8 --> <!-- force standards mode when available -->
	<style>
		* {
			padding:0;
			margin:0;
		}
	
		body.login {
			color:#fff;
			background-color: #000;
			/* background-color:#333029; */
			font-family: verdana;
			font-size:12px;

		}
		
		body.login input[type=text],body.login input[type=password]{
			background-color: #eFeFeF;
			border: 1px solid #ccc;
			padding:4px 0px 4px 0px;
			width: 100%;
			color:#000;
		}
		
		body.login table.login {
			width: 320px;
			border-collapse:collpase;
		}
		
		body.login table.login td {
			padding:4px;
		}
		
		body.login img.logo {
			padding:12px 0px 8px 0px;
		}
		
		body.login .label-td {
			vertical-align:middle;
		}

		body.login .submit-td {
			vertical-align:top;
		}
		
		body.login .label-td, body.login .submit-td {
			text-align: right;
			color:white;
		}
		
		body.login .label-td, body.login .submit-td input {
			padding:3px 6px 3px 6px;
		}
		
	</style>
</head>
<body class="login admin">
	<form method="POST">
		<table width="100%" height="68%">
			<tr>
				<td valign="middle" align="center">
					<table class="wrapper login">
						<tr>
							<td>
							</td>
							<td align="left">
								<img class="logo" src="<?= USRROOT ?>img/logo.png">
							</td>					
						</tr>
						<tr>
							<td class="label-td">
								Username:
							</td>
							<td class="input-td">
								<input type="text" name="username">
							</td>
						</tr>		
						<tr>
							<td class="label-td">
								Password:
							</td>
							<td class="input-td">
								<input type="password" name="password">
							</td>
						</tr>
						<tr>
							<td colspan="2" class="submit-td">
								<input type="submit" value="Login">
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<? isset($message) and !empty($message) and print($message) ?>
							</td>		
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<input type="hidden" name="model" value="login" />
	</form>
</body>
</html>


<?php

die();	

?>