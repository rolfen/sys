// loadQueuedImages
function imageQueueLoader() {
	'use strict';

	/* image loading code adapted from zankoulizer.me */

	// image queuing
	var imageQueue;
	var imageLoader = this;

	/* not used anymore
	this.isImageLoaded = function(img) {
	    // During the onload event, IE correctly identifies any images that
	    // weren’t downloaded as not complete. Others should too. Gecko-based
	    // browsers act like NS4 in that they report this incorrectly.

	    if (!img.complete) {
	        return false;
	    }

	    // However, they do have two very useful properties: naturalWidth and
	    // naturalHeight. These give the true size of the image. If it failed
	    // to load, either of these should be zero.

	    if (typeof img.naturalWidth != "undefined" && img.naturalWidth == 0) {
	        return false;
	    }

	    // No other way of checking: make an assumption
	    return(true);
	}
	*/

	this.gotoNext = function(currentImage) {
		// goes to next image to load in the queue and calls loadImage();
		if (imageQueue.length > 0) {	
			//setTimeout(2000,
			//	function(){
					imageLoader.loadImage(imageQueue.shift(), imageLoader.gotoNext);
			//	}
			//);
		}
	}

	this.loadImage = function(image, callback) {
		if(typeof callback != 'function') {
			callback = function(bogusArg){
				// do nothing
			}
		}
		// loads given image
		//
		// if (typeof image != 'undefined' && image && !imageLoader.isImageLoaded(image)) {
		// nah skip isImageLoaded() and rely on the onload event. I think isImageLoaded was necessary for old IE versions only.
		if (typeof image != 'undefined' && image) {
			var qsrc;
			image.onload = function(evt){
				evt = evt ? evt : this; // IE8
				// when done loading, go to next
				if(hasClass(image, "preloader")) {
					image.parentNode.setAttribute("iql-status","loaded");
					image.parentNode.removeChild(image); // this.parentNode.removeChild(this)... hahahahaha!
				} else {				
					image.setAttribute("iql-status","loaded");
				}
				callback(evt.target);
			}
			qsrc = image.getAttribute("qsrc");
			image.setAttribute("src", qsrc);
			image.setAttribute("iql-status","loading");
			image.removeAttribute("qsrc");
			if(hasClass(image, "preloader")) {
				// img with "preloader" class will set parent background to it's src attribute then delete itself
				image.parentNode.style['backgroundImage'] = 'url('+qsrc+')';
			}
		} else if(typeof image == 'undefined') {
			// console.log("this is not an image");
		} else {
			// console.log("THIS SHOULD NEVER HAPPEN");
		}
	}

	this.initImageQueue = function() {
		if(typeof imageQueue == 'undefined') {
			imageQueue = NW.Dom.select("img[qsrc]",document);
		}
	}

	this.spawn = function(threads) {
		imageLoader.initImageQueue();
		while(threads > 0) {
			imageLoader.gotoNext();
			threads--;
		}
	}

	this.loadAllAtOnce = function() {
		NW.Dom.select("img[qsrc]",document, imageLoader.loadImage);
	}
}