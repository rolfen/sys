function nextElement(n) {
	do n = n.nextSibling;
	while (n && n.nodeType != 1);
	return n;
}
function previousElement(p) {
	do p = p.previousSibling;
	while (p && p.nodeType != 1);
	return p;
}

function preloadImg(src) {
	// preloads image so that it is ready and in the cache even if not showing yet
	if(src instanceof Array) {
		pics = new Array();
		src.forEach(function(itemSrc){
			var pic = new Image();
			pic.src = itemSrc;
			pics.push(pic);
		});
	} else {
		var pic = new Image();
		pic.src = src;
	}
}

function documentScrollTop(val) {
	// FF / Chrome compat workaround
	// if given value, will scroll to that position, otherwise just return current position	
	var scrollTop = (document.body.scrollTop) ?  document.body.scrollTop :  document.documentElement.scrollTop;
	if(val) {
		window.scroll(0, parseInt(val));
	}
	return(scrollTop)
}

/*!
 * contentloaded.js
 *
 * Author: Diego Perini (diego.perini at gmail.com)
 * Summary: cross-browser wrapper for DOMContentLoaded
 * Updated: 20101020
 * License: MIT
 * Version: 1.2
 *
 * URL:
 * http://javascript.nwbox.com/ContentLoaded/
 * http://javascript.nwbox.com/ContentLoaded/MIT-LICENSE
 *
 */

// @win window reference
// @fn function reference

function setContentLoadedCallback(win, fn) {

	var done = false, top = true,

	doc = win.document, root = doc.documentElement,

	add = doc.addEventListener ? 'addEventListener' : 'attachEvent',
	rem = doc.addEventListener ? 'removeEventListener' : 'detachEvent',
	pre = doc.addEventListener ? '' : 'on',

	init = function(e) {
		if (e.type == 'readystatechange' && doc.readyState != 'complete') return;
		(e.type == 'load' ? win : doc)[rem](pre + e.type, init, false);
		if (!done && (done = true)) fn.call(win, e.type || e);
	},

	poll = function() {
		try { root.doScroll('left'); } catch(e) { setTimeout(poll, 50); return; }
		init('poll');
	};

	if (doc.readyState == 'complete') fn.call(win, 'lazy');
	else {
		if (doc.createEventObject && root.doScroll) {
			try { top = !win.frameElement; } catch(e) { }
			if (top) poll();
		}
		doc[add](pre + 'DOMContentLoaded', init, false);
		doc[add](pre + 'readystatechange', init, false);
		win[add](pre + 'load', init, false);
	}

}


function animated(callback,from,to,time,interval,completedCallback) {
	'use strict';
	// interval is optional, defaults to 25 ms
    if(!callback) return;
    if(!interval) interval = 25; // ms
    var start = new Date().getTime(),
        timer = setInterval(function() {
            var step = Math.min(1,(new Date().getTime()-start)/time);
            callback((from+step*(to-from)));
			if( step == 1) {
				clearInterval(timer);
				if(completedCallback) completedCallback();
			}
		},interval);
	callback(from);
	// return timer so that we can stop execution prematurely if needed
	return(timer);
}

// credit http://stackoverflow.com/a/384380/370786

//Returns true if it is a DOM node
function isNode(o){
  return (
    typeof Node === "object" ? o instanceof Node : 
    o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName==="string"
  );
}

//Returns true if it is a DOM element    
function isElement(o){
  return (
    typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
    o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
);
}

// source (now heavily modified): http://www.avoid.org/javascript-addclassremoveclass-functions/

function hasClass(el, name) {
	"use strict";
	if(isElement(el)) {
		return new RegExp('(\\s|^)'+name+'(\\s|$)').test(el.className);
	}
}

function addClass(el, name){
	'use strict';
   if (isElement(el) && !hasClass(el, name)) { 
   		el.className += (el.className ? ' ' : '') +name; 
   	}
}

function removeClass(el, name){
	'use strict';
   if (hasClass(el, name)) {
      el.className=el.className.replace(new RegExp('(\\s|^)'+name+'(\\s|$)'),' ').replace(/^\s+|\s+$/g, '');
   }
}

function toggleClass(el, name) {
	'use strict';
	var hadTheClass;
	if(hadTheClass = hasClass(el, name)) {
		removeClass(el, name);
	} else {
		addClass(el, name);
	}
	return(!hadTheClass);
}

function arrayEncodeURI(arr) {
	var URI = '';
	for(prop in arr) {
		if(arr.hasOwnProperty(prop)){
			URI += encodeURI(prop) + '=' + encodeURI(arr[prop]) + '&';
		}
	}
	if(URI.length > 0) {
		URI = URI.substring(0, URI.length - 1);
	}
	return(URI);
}

function parseForm(form) {
	var formData = {};
    NW.Dom.select("input[name]:not([type=submit]), textarea[name], select[name]", form, function(el){
		// postStr += '&' + el.getAttribute('name') +'='+ encodeURI(el.value);
		formData[el.getAttribute('name')] = el.value;
    });
    return(formData);
}

function validateForm(form, onFail, onSuccess) {
	// test every element
	var validationPassed = true;
    var errorMessage;
    NW.Dom.select("input[name]:not([type=submit]), textarea[name], select", form, function(el){
    	errorMessage = undefined;
    	console.dir(el.value);
    	if(!el.value || el.value == '') {
    		// empty input
			if(el.hasAttribute('require_anything')) {
				errorMessage = 'Field "'+el.getAttribute('name')+'" must be non-empty.';
			}
    	} else {
    		// validate input data format		
			if (el.hasAttribute('require_email') && !/.+@.+/.test(el.value)) {
				errorMessage = 'Field "'+el.getAttribute('name')+'" must be an email address.';
			};
			if (el.hasAttribute('require_extension')) {
				var allowedExtensions = el.getAttribute('require_extension').split(',')
				var reStr = '\.('+allowedExtensions.join('|')+')$'
				var re = new RegExp(reStr,'i')
				if(!re.test(el.value.trim())){
					errorMessage = 'Field "'+el.getAttribute('name')+'" only accepts files with the following extensions: '+el.getAttribute('require_extension')
				} 
			}
    	}
		if(typeof errorMessage != 'undefined' && onFail) {
			validationPassed = false;
			onFail(el, errorMessage);
		}
    });
    if(validationPassed) {
    	var validationInput = document.createElement("input");
    	validationInput.setAttribute("type","hidden");
    	validationInput.setAttribute("name","validated");
    	validationInput.setAttribute("value","1");    	
    	form.appendChild(validationInput);
    	return(onSuccess(form));
    } else {
	    return(errorMessage);
    }
}

function ajaxRequest(handler, filters, callback, request) {
	// filters are sent as GET args, request is sent as POST
	if(!filters) {filters = []}
	if(!request) {request = {}}
	// build URL query string
	var filterString = handler+"?"+arrayEncodeURI(filters);
	/*
	for(prop in filters) {
		if(filters.hasOwnProperty(prop)){
			filterString += encodeURI(prop) + '=' + encodeURI(filters[prop]) + '&';
		}
	}
	filterString = filterString.replace(/&$/,'');
	*/

	// set up ajax interface
	var xhReq = new XMLHttpRequest();
	xhReq.onreadystatechange = function(){
		if (xhReq.readyState == 4) {
			if (xhReq.status == 200) {
				// ajax success
				if(xhReq.responseText) {
					if(callback) {
						callback(xhReq.responseText)
						return(true)
					}
				} else {
					// empty response?
					alert("Ajax error: Empty server response. Your message might not have been sent.")
					return(false)
		 		}
			} else {
				// oopsie
				alert("Ajax error: Non-success server response. Your message might not have been sent.")
				return(false)
			}
		}
	}

	// do request
	var requestStr = arrayEncodeURI(request);
	if(requestStr.length > 0) {
		xhReq.open("POST", filterString, true);
		xhReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhReq.send(requestStr);				
	} else {
		xhReq.open("GET", filterString, true);
		xhReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhReq.send();				
	}
}


/**
 * Fire an event handler to the specified node. Event handlers can detect that the event was fired programatically
 * by testing for a 'synthetic=true' property on the event object
 * @param {HTMLNode} node The node to fire the event handler on.
 * @param {String} eventName The name of the event without the "on" (e.g., "focus")
 */
function fireEvent(node, eventName) {
    // Make sure we use the ownerDocument from the provided node to avoid cross-window problems
    var doc;
    if (node.ownerDocument) {
        doc = node.ownerDocument;
    } else if (node.nodeType == 9){
        // the node may be the document itself, nodeType 9 = DOCUMENT_NODE
        doc = node;
    } else {
        throw new Error("Invalid node passed to fireEvent: " + node.id);
    }

     if (node.dispatchEvent) {
        // Gecko-style approach (now the standard) takes more work
        var eventClass = "";

        // Different events have different event classes.
        // If this switch statement can't map an eventName to an eventClass,
        // the event firing is going to fail.
        switch (eventName) {
            case "click": // Dispatching of 'click' appears to not work correctly in Safari. Use 'mousedown' or 'mouseup' instead.
            case "mousedown":
            case "mouseup":
                eventClass = "MouseEvents";
                break;

            case "focus":
            case "change":
            case "blur":
            case "select":
                eventClass = "HTMLEvents";
                break;

            default:
                throw "fireEvent: Couldn't find an event class for event '" + eventName + "'.";
                break;
        }
        var event = doc.createEvent(eventClass);

        var bubbles = eventName == "change" ? false : true;
        event.initEvent(eventName, bubbles, true); // All events created as bubbling and cancelable.

        event.synthetic = true; // allow detection of synthetic events
        // The second parameter says go ahead with the default action
        node.dispatchEvent(event, true);
    } else  if (node.fireEvent) {
        // IE-old school style
        var event = doc.createEventObject();
        event.synthetic = true; // allow detection of synthetic events
        node.fireEvent("on" + eventName, event);
    }
};
