
function debug(variable) {
    console.log(variable);
    return(variable);
}

// function passed to window.onbeforeunload
var changeFunc = function() {
    return 'You have unsaved changes! Are you sure you want to leave this page?';
}

// cropbox selection callback
var selected = function (img, selection) {
    //alert($(img).attr('src'));
	var cropbox_name = $(img).attr("name");
	var x1 = $('input[name="'+cropbox_name+'[x1]"]').attr("value", selection.x1);  
	var y1 = $('input[name="'+cropbox_name+'[y1]"]').attr("value", selection.y1);  
	var x2 = $('input[name="'+cropbox_name+'[x2]"]').attr("value", selection.x2);  
	var y2 = $('input[name="'+cropbox_name+'[y2]"]').attr("value", selection.y2);  
	var w = $('input[name="'+cropbox_name+'[w]"]').attr("value", selection.width);  
	var h = $('input[name="'+cropbox_name+'[h]"]').attr("value", selection.height);  
	window.onbeforeunload = changeFunc;			
	if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){  
		alert("You must make a selection first");
		return false;
	}else{  
		return true;  
	}
};

$( window ).load(function() {
	// all resources (images, etc) are loaded

	// init cropbox
	$('img.cropbox').each(function(){
		var cropbox_name = $(this).attr("name");
		var cropbox_initial = new Array();
		var aspect_ratio = $(this).attr("aspect_ratio");
		cropbox_initial['x1']=parseInt($('input[name="'+cropbox_name+'[x1]"]').attr("value"));
		cropbox_initial['x2']=parseInt($('input[name="'+cropbox_name+'[y1]"]').attr("value"));
		cropbox_initial['y1']=parseInt($('input[name="'+cropbox_name+'[x2]"]').attr("value"));
		cropbox_initial['y2']=parseInt($('input[name="'+cropbox_name+'[y2]"]').attr("value"));
        $(this).imgAreaSelect({
    		aspectRatio: aspect_ratio,
    		handles: true,
    		x1: cropbox_initial['x1'],
    		y1: cropbox_initial['x2'],
    		x2: cropbox_initial['y1'],
    		y2: cropbox_initial['y2'],
    		onSelectEnd: selected
    	});
	});
});

$(function(){
	// DOM is ready



	$(".richtext").each( function() {
		var richtext = this;
		designMode="on";
		$(richtext).attr("contenteditable","true");
		try {
			document.execCommand("styleWithCSS", false, false);
		} catch(err) {
			// maybe this is IE...
		}
		// richtext.contentEditable = true;
		if ($(richtext).html() == "") {
			// hidden UTF character... firefox bug workaround
			// $(richtext).html("\xE2\x80\x8B");
		}
		$(richtext).closest("form").bind('submit', function(){
			$(this).append(
				'<textarea style="visibility:hidden;display:none;" name="' + $(richtext).attr("name") + '">' + $(richtext).html() + '</textarea>'
			);				
		})
	});

	$("input[date]").each(function(){
		//$(this).AnyTime_picker({ format: "%Y-%M-%D"}); // doesnt work! see workaround below with random_id
		var random_id = "id" + Math.floor(Math.random()*9000)
		var format = ( $(this).attr("format") ? $(this).attr("format") : "%Y-%m-%d" ); 
		$(this).attr("id",random_id);
		AnyTime.picker(random_id,{ 
			'format': format
			
			//'earliest': "1900-01-01"

		});
	});

    var cssApplier;
    rangy.init();
    cssAppliers = new Array();

   	$("[rt_command]").bind("click", function(){
		var command = $(this).attr("rt_command");
		if (command == "link") {
			link_url = prompt("Please enter address \nFor external web links: start with: \"http://\" \nFor email: start with: \"mailto:\" \n ","");
        	document.execCommand('createLink', false, link_url);
		} else if (command == "email_link") {
			link_url = prompt("Please enter email address after \"mailto:\" ","mailto:");
        	document.execCommand('createLink', false, link_url);
		} else if (command == "web_link") {
			link_url = prompt("Please enter complete webpage URL (for external links)","http://");
        	document.execCommand('createLink', false, link_url);      
		} else if (command == "unlink" || command == "bold" || command == "italic") {
			document.execCommand(command, false, null);
		} else {
			// use rangy
			var selection = rangy.getSelection();
			if( ($(selection.anchorNode).parents(".textarea.richtext").length > 0) && ($(selection.focusNode).parents(".textarea.richtext").length > 0) ) {
				if (!cssAppliers[command]) {
					// create "applier" if not exist
					cssAppliers[command] = rangy.createCssClassApplier(command, {normalize: true}) ;
				} 
				cssAppliers[command].toggleSelection();
			} else {
				// we're outside the textarea, do nothing I guess
				alert("You have not selected any text in the edit box!");
			}
		}
		return(false);
	})

    /*
	$("[rt_command]").bind("click", function(){
		var command = $(this).attr("rt_command");
		if (command == "link") {
			link_url = prompt("Please enter address","");
        	document.execCommand('createLink', false, link_url);
        } else if (command == "blockquote") {
        	document.execCommand("formatblock", false, "blockquote");
		} else {
			document.execCommand(command, false, null);
		}
		return(false);
	})
	*/

	// go into sorting mode
	$(".dragndrop").sortable({
		"cancel" : ".nodrag",
		"update" : function(e){
			var list = [];
			$("[data_id]", e.target).each(function(){
				list.push($(this).attr("data_id"));
			});
			var handler = e.target.getAttribute("sort_handler");
			console.log(list);
			jQuery.ajax({
				"url" : handler,
				"type" : "POST",
				"async" : false,
				"data" : {"d":list.join(',')}
			})
		}
	});

	$("a.fancypopup").fancybox({
		 // 'autoScale' : false,
		 'width': 600,
		 'height': '90%',
		 'type': 'iframe',
		 'hideOnOverlayClick' : false,
		 'onCleanup' : function(elem){
		 	// let's try to avoid re-submitting POST
			window.location.replace(window.location.href);
		 	//window.location.reload();
		 }
	 });

	var firstInput;
	if(firstInput = $("form input, form textarea, form select").get(0)) {

		// set cursor in first form input
		firstInput.focus();

		// leave page with unsaved changes confimation
		$(".richtext").keyup(function(e){
			window.onbeforeunload = changeFunc;
		});
		$("form input,form textarea, form select").change(function(e){
			window.onbeforeunload = changeFunc;
		});
	}

	// submit must not trigger "leave page warning"
	$("input[type=submit]").click(function(){
		window.onbeforeunload = null;
	});

   	// chosen seems to render with an improper width when called too early
	setTimeout( function() { $(".chzn").chosen(); }, 200 );

});