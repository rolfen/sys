Sys
====

What is it?
-----------

A framework for building a website with a bespoke CMS, based on PHP and MySQL.
It is meant to be re-useable, minimal, helpful and mostly used for building small to medium complexity "professional web presence" type websites for companies and individuals.

It started as some code being reused across my freelance projects, going back as far as 2010. For most of it's life so far, this codebase only had a single user and maintainer (myself).

There are various issues that make it difficult to extend this framework beyond it's original scope, for which I am receiving little interest at the moment.

In any case, it can do a couple of neat things now, see "features" below.

Status
------

Unfortunately, the project is not active at the moment.

Also I am having trouble understanding the part of the code in generic.php.

I still use and re-use the rest, though, so maybe I can document this project without the "dynamic CMS" functionality.

How to use it?
--------------

Please see example.md for more details.

The aim is to keep all the re-usable codebase in a single directory, the "sys" directory. This directory would sit at, say, apache document root (possibly shared across different websites). It can be hosted remotely and  loaded over HTTP as well. 


Cool things
===========


Dynamic CMS
-----------

Sys contains code to dynamically generate a CMS interface from a "models" definition. That's one of the neat things it does. Here is how it works:

1. Create the tables in the database. This is a manual step that can be automated in the future, because it is staight-forward to derive a database structure from the model definitions.  
![tables](https://bytebucket.org/rolfen/sys/raw/master/readme/tables.png "The Tables")

2. Define the "models". Basically describe which table and table fields we want editable in the CMS, how they relate to each other and how to edit them.  
![model](https://bytebucket.org/rolfen/sys/raw/master/readme/model.png "The Model")

3. The result: fully functional (CRUD), useable bespoke CMS, with user-friendly controls, such as a calendar for picking dates and an image crop tool.  
![result](https://bytebucket.org/rolfen/sys/raw/master/readme/CMS.png?at=master "The Result")



Useful functions
----------------

The lib/funcs.php file contains a handful of rather generic function which can be useful.  One of them, for example is `function sync_query($record, $table, $id_column = "id")`. It essentially generates an "INSERT ... ON DUPLICATE REPLACE" query based on the passed object, for the specified table.

