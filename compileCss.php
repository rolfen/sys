<? 

/* WARNING  WARNING  WARNING !!
 * CSS will be automatically generated
 * Any changes in compiled files will be overwritten
 * do your changes in the source CSS file
 */

error_reporting(E_ALL);

// Modify source and target below according to your needs

defined(USRROOT) or define(USRROOT,'.');

$SourceFile = USRROOT.'/css/user.css';
$TargetFile = USRROOT.'/css/user-compiled.css';

print("<pre>\n");

print("Hello, I am the CSS compiler. \n");

print(file_put_contents(
	$TargetFile,
	compileCssFile($SourceFile)
).". \n");
print("I think we're done here. \n");

print("</pre>\n");

print("<a href='$SourceFile'>View Source CSS</a><br/>\n");
print("<a href='$TargetFile'>View Compiled CSS</a><br/>\n");

// functions  and classes are below

function remToPx($m, $mult = 0.9) {
    // $ret = round($m[1] * $mult, 0, PHP_ROUND_HALF_ODD) . 'px' . $m[3];
    $ret = ceil($m[1] * $mult) . 'px' . $m[3];
    // print_r($m);
    // echo "\r\n mult($mult) => $ret \r\n";
    return($ret);
}

function compileCss($css) {
	print("compileCss(\$css)\n");
	/*
	$tmpCss = preg_replace_callback ( 
	    '/([0-9.]+)(rem)(\W)/' ,
	    'remToPx' , 
	    $css
	);
	*/
	$ThisMouse = new Mouse($css);
	return($ThisMouse->FillPlaceholders($ThisMouse->css));
}

function compileCssFile($filename) {
	return(compileCss(file_get_contents($filename)));
}

// classes are below

Class Mouse {
	public $css,$variables;
	public $VariablesReplaced = 0;

	function __construct($css) {
		print("Hello, I am Mouse\n");
		$this->css = $css;
		$this->variables = $this->CssVariables($css);
	}

	function CssVariables($css) {
		/* returns array of parsed <<Variable>> instances. */
		$variablesRegexMatches = $variables = array();
//		$matches = preg_match_all('/^\s*([a-zA-Z0-9])\s*<<(.*)$/',$css,$variablesRegexMatches,PREG_SET_ORDER);
		$matches = preg_match_all('/(.*)<<(.*)/',$css,$variablesRegexMatches,PREG_SET_ORDER);
		print("Mouse::CssVariables, preg_match_all() regex matches: ");
		print_r($variablesRegexMatches);
		print("\n");
		foreach($variablesRegexMatches as $regexSubMatch ) {
			/* $regexSubMatch[0] contains the whole match */
			$variables[$regexSubMatch[1]] = rtrim($regexSubMatch[2],"\r\n");
		}
		print("Mouse::CssVariables(\$css) found ".$matches." and parsed ".count($variables)." variables.\n");
		return($variables);
	}

	function ComputeNumericInstruction($variable, $instruction, $data) {
		if(is_numeric($variable)) {
			// we have a number we can work with
			switch($instruction) {
				case "+":
					$computed = (int) $variable + (int) $data ;
				break;
				case "-":
					$computed = (int) $variable - (int) $data;
				break;
				case "/":
					$computed = (int) $variable / (int) $data;
				break;
				default:
					print("Instruction $instruction not understood");
					return($variable);
				break;
			}
			return($computed);
		} else {
			print("Cannot compute $variable with instruction $instruction");
			return($variable);
		}
	}

	function FillPlaceholders_RegexMatch($m) {
		/* This function is a callback to be called 
		 * by preg_replace_callback() in FillPlaceholders() 
		 */
		print("Mouse::FillPlaceholders_RegexMatch(\$m) with \$m = ");
		print_r($m);
		// fetch interesting variables in the $m matches array
		$placeHolderMatchIndex = 1;
		$instructionMatchIndex = 2;
		$dataMatchIndex = 3;
		$matchedString = $m[0];
		if(isset($m[$instructionMatchIndex])) {
			$instruction = $m[$instructionMatchIndex]; 
			$data = $m[$dataMatchIndex]; // data attached to the instruction
			// process instructions if any
			print("instruction=$instruction\n");
		}
		$placeHolder = $m[$placeHolderMatchIndex]; // the name of the placeholder, actually

		$dataIsProcessable = isset($data)
			and isset($this->variables[$placeHolder])
			and is_numeric($data)
			and is_numeric($this->variables[$placeHolder])
		;
		if($dataIsProcessable) {
			// break down variable into numeric and non-numeric part
			$tmpMatches = preg_match(
				"/^([^0-9]*)([0-9]+)([^0-9]*)$/"
				, $this->variables[$placeHolder]
				, $variableBreakDown
			);
			$variableHead = $variableBreakDown[1];
			$variableNumericPart = $variableBreakDown[2];
			$variableTail = $variableBreakDown[3];
			if(is_numeric($variableNumericPart)) {
				// we have a number we can work with
				$computed = $this::ComputeNumericInstruction($variableNumericPart, $instruction, $data);
				$retval = 
					$variableHead 
					. $computed
					. $variableTail
				;				
			}
		} else {
			print("data is not processable\n");
		}
		// by default (if no instruction has set $retval already), this is what happens:
		// return the translated line only if we have this variable, otherwise the whole match (unchanged).
		$retval = isset($retval) 
			? $retval 
			: ( 
				isset($this->variables[$placeHolder]) 
					? $this->variables[$placeHolder] 
					: $matchedString 
				)
		;
		// for logging: detect replacement and log it
		($retval == $this->variables[$m[$placeHolderMatchIndex]]) and $this->VariablesReplaced++;
		print("retval=$retval\n");
		print("----------------------------------------------\n");
		return($retval);
	}

	function FillPlaceholders($css) {
		/* Looks for <PlaceHolder> instances in css.
		 * Triggers one callback for every regex match.
		 */
		$this->VariablesReplaced = 0;
		print("Mouse::FillPlaceholders(\$css)\n");
		$retval = preg_replace_callback ( 
		    '/<([a-zA-Z0-9]+)(?:([+-\/])([0-9])+){0,1}>/' ,
		    'Mouse::FillPlaceholders_RegexMatch',
		    $css
		);
		print("Mouse::FillPlaceholders(\$css) filled ".$this->VariablesReplaced." placeholders.\n");
		return($retval);
	}

}





?>
