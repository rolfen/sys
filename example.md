For this example, we will create a product gallery, with a description, price and multiple images for each product.

We need to have MySQL (or compatible) installed, and a way to serve interpreted PHP content. Normally that would be Apache with  PHP, but for testing, a recent version of PHP with a built-in server will suffice. It is assumed in the instructions below that these servers run on localhost.

First, we need the code, cloned from the repository:

	git clone https://github.com/rolfen/sys.git
	cd sys

Let's create the database for this exercice. You will need a MySQL client to execute the following queries. I normally use HeidiSQL as a graphical MySQL client. 

    CREATE DATABASE `sys_example_gallery`;
	USE `sys_example_gallery`;

Then the `user` table for the CMS login.

	CREATE TABLE IF NOT EXISTS `user` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `username` varchar(128) NOT NULL,
	  `password` varchar(128) NOT NULL,
	  `lastfailedattempt` INT(11) NULL DEFAULT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB AUTO_INCREMENT=1

And while at it, create a CMS user named 'admin', with password 'admin'

	INSERT INTO `sys_example_gallery`.`user` 
	  SET	`username` = 'admin',
			`password` = SHA1('admin');


We should create a MySQL user and grant it access to the database.

	CREATE USER 'sys_example'@'localhost' IDENTIFIED BY 'ButterPassword';
	GRANT USAGE ON *.* TO 'sys_example'@'localhost';
	GRANT ALL PRIVILEGES ON `sys_example_gallery`.* TO 'sys_example'@'localhost' WITH GRANT OPTION;

Let's connect the code to the database. Add the following in app.php, under "connextion settings":

	mysql_connect('localhost', 'sys_example', 'ButterPassword');
	mysql_select_db('sys_example_gallery');

Now would be a good time to test the admin login. If you don't have a web server properly configured, you can use the embedded PHP server. From a terminal window, in the sys root folder, The following command will run an HTTP server on port 8080. Short open tags are used through the code, so we need to make sure this option is enabled:

	php -S localhost:8080 -d short_open_tag=1

Fire up your favorite browser and navigate to:

	http://localhost:8080/admin.php

You should be able to log in using the username 'admin' and password 'admin' (as previously added to our user table).

Create the tag, photo, and tag2photo tables:

	CREATE TABLE `tag` (
		`id` INT(11) NOT NULL AUTO_INCREMENT,
		`name` VARCHAR(250) NULL DEFAULT NULL,
		PRIMARY KEY (`id`)
	)
	ENGINE=InnoDB;

	CREATE TABLE `photo` (
		`id` INT(11) NOT NULL AUTO_INCREMENT,
		`caption` VARCHAR(250) NULL DEFAULT NULL,
		`img_original` LONGBLOB NULL,
		`img_web` MEDIUMBLOB NULL,
		PRIMARY KEY (`id`)
	)
	ENGINE=InnoDB;
